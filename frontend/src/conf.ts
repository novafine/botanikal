const BACKEND_PORT = '4000';
export const BACKEND_URL = `https://node59.cs.colman.ac.il:${BACKEND_PORT}`;

export const ROUTES = {
    //auth routes
    LOGIN_USER: `${BACKEND_URL}/auth/login`,
    REGISTER_USER: `${BACKEND_URL}/auth/register`,
    LOGOUT_USER: `${BACKEND_URL}/auth/logout`,
    GOOGLE_LOGIN: `${BACKEND_URL}/auth/google`,
    REFRESH_TOKENS: `${BACKEND_URL}/auth/refresh`,

    //post routes
    GET_ALL_POSTS: `${BACKEND_URL}/posts`,
    UPLOAD_POST: `${BACKEND_URL}/posts/upload`,
    GET_SINGLE_POST: (postId: string) => `${BACKEND_URL}/posts/${postId}`,
    EDIT_POST: `${BACKEND_URL}/posts/edit`,
    DELETE_POST: (postId: string) => `${BACKEND_URL}/posts/${postId}`,
    ADD_COMMENT: `${BACKEND_URL}/posts/comment`,

    //user routes
    GET_USER: (username: string) => `${BACKEND_URL}/user/${username}`,
    UPDATE_USER: `${BACKEND_URL}/user/edit`,
    UPDATE_PROFILE_PICTURE: `${BACKEND_URL}/user/editProfilePicture`,
    GET_USER_BY_USERNAME: (username: string) => `${BACKEND_URL}/user/${username}`,
    GET_ALL_POSTS_OF_USER: (username: string) => `${BACKEND_URL}/posts/byUser/${username}`,

    //plants routes
    GET_ALL_PLANTS_NAMES: `${BACKEND_URL}/plants/plantsNames`,
    GET_RANDOM_PLANT: `${BACKEND_URL}/plants/randomPlant`
}

export const getAuthHeader = () => {
    const token = `JWT ${getTokenFromStorage()}`;
    return { token, header: { headers: { 'Authorization': token } } }
}

export const getTokenFromStorage = () => {
    return localStorage.getItem('token');
}

export const getRefreshTokenFromStorage = () => {
    return localStorage.getItem('refreshToken');
}
