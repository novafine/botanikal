import { AddOutlined } from "@mui/icons-material"
import { Fab } from "@mui/material"
import { useNavigate } from "react-router-dom"
import { routes } from "../../routes/router";
import { themeButton } from "../../common-styles";

function UploadPostBtn() {
    const navigate = useNavigate();

    const goToUploadPost = () => {
        navigate(routes.uploadPostPage);
    }

    return (
        <>
            <Fab color="secondary" aria-label="add" onClick={goToUploadPost}
            sx={themeButton}>
                <AddOutlined />
            </Fab>
        </>
    )
}

export default UploadPostBtn
