import styles from './Login-form.module.css'
import { Button, TextField } from '@mui/material';
import { textFieldStyle, themeButton } from '../../common-styles';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { UserService } from '../../services/User-service';
import { useUser } from '../../providers/User-provider';
import { useSnackbar } from '../../providers/Snackbar-provider';
import { useNavigate } from 'react-router-dom';
import { routes } from '../../routes/router';

export interface LoginData {
    email: string;
    password: string;
}

const LoginForm = () => {
    const { setCurrUser } = useUser();
    const { openSnackbar } = useSnackbar();
    const navigate = useNavigate();

    const validationSchema = Yup.object().shape({
        email: Yup.string().required('Email is required').test('email-format', 'Invalid email format', (value) => {
            return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value);
        }),
        password: Yup.string().required('Password is required'),
    });

    const formik = useFormik({
        initialValues: {
            email: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: async (values: LoginData, { resetForm }) => {
            await UserService.loginUser(values).then((res) => {
                if (res.data) {
                    setCurrUser(res.data);
                    resetForm();
                    navigate(routes.homePage);
                }
            })
                .catch((error) => {
                    openSnackbar(error.response.data);
                });
        },
    });

    return (
        <>
            <form className={styles.formContainer} onSubmit={formik.handleSubmit}>
                <TextField
                    className={styles.textField}
                    label="Email"
                    name="email"
                    type="email"
                    variant="standard"
                    value={formik.values.email}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.email && Boolean(formik.errors.email)}
                    helperText={formik.touched.email && formik.errors.email}
                    sx={textFieldStyle}
                    required
                />
                <TextField
                    className={styles.textField}
                    label="Password"
                    name="password"
                    type="password"
                    variant="standard"
                    value={formik.values.password}
                    onChange={formik.handleChange}
                    onBlur={formik.handleBlur}
                    error={formik.touched.password && Boolean(formik.errors.password)}
                    helperText={formik.touched.password && formik.errors.password}
                    sx={textFieldStyle}
                    required
                />
                <Button type="submit" variant="contained" sx={themeButton}>
                    login
                </Button>
            </form>
        </>
    )
}

export default LoginForm