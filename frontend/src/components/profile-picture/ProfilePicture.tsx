import styles from './ProfilePicture.module.css';

const ProfilePicture = ({ imageUrl, size }: { imageUrl: string; size: number; }) => {
    return (
        <img src={imageUrl} alt='profile picture' width={size + 'px'} height={size + 'px'} className={styles.picture} /> // TODO: use Avatar instead
    );
};

export default ProfilePicture;