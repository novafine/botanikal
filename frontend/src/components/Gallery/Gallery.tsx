import { ImageList } from "@mui/material"
import { FunctionComponent } from "react";
import styles from './Gallery.module.css'
import Post, { PostMetadata } from "../post/post";
import { useNavigate } from "react-router-dom";

interface GalleryProps {
    posts: PostMetadata[];
}

const Gallery: FunctionComponent<GalleryProps> = props => {
    const navigate = useNavigate();
    const { posts } = props;
    const onClickFunc = (postId: string) => {
        navigate(`/post/${postId}`)
    }

    return (
        <>
            <ImageList cols={4} className={styles.postsContainer} gap={10}>
                {posts.map((post: PostMetadata) => (
                        <Post 
                        post = {post} 
                        onClickFunc = {onClickFunc}>
                        </Post>
                ))}
            </ImageList>
        </>
    );
};

export default Gallery;