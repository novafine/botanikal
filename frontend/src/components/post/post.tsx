import { ImageListItem, ImageListItemBar } from "@mui/material"
import styles from './post.module.css'
import { ModeCommentOutlined } from "@mui/icons-material";
import { BACKEND_URL } from "../../conf";

export interface PostMetadata {
    _id: string
    publisher: string;
    comments: Comment[];
    imagePath: string;
    plantType: string;
    description: string,
}

export interface Comment {
    commenter: string,
    description: string;
}

export interface PostProperties {
    post: PostMetadata
    onClickFunc: (postId: string) => void
}

const Post = (props: PostProperties) => {
    const { post, onClickFunc } = props;

    return (
        <>
            <ImageListItem key={post._id} className={styles.singlePost} onClick={() => onClickFunc(post._id)}>
                <img src={`${BACKEND_URL}/${post.imagePath}`} />
                <ImageListItemBar
                    title={`@${post.publisher}`}
                    classes={{
                        root: styles.titleBar,
                        actionIcon: styles.actionIcons,
                        titleWrap: styles.titleWrap
                    }}
                    actionIcon={
                        <>
                            {post.comments.length} <ModeCommentOutlined />
                        </>
                    }
                />
            </ImageListItem>
        </>
    )
}

export default Post