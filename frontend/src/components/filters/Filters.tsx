import { Autocomplete, InputAdornment, TextField } from "@mui/material";
import { debounce } from "lodash";
import React, { FunctionComponent, useEffect, useState } from "react";
import { textFieldStyle } from "../../common-styles";
import styles from './Filters.module.css';

interface FiltersProps {
    allPlantsNames: string[];
    onFilterChange: (filterOptions: FilterOptions) => void;
}

export interface FilterOptions {
    publisher: string;
    plantType: string;
}
const Filters: FunctionComponent<FiltersProps> = props => {
    const { allPlantsNames, onFilterChange } = props
    const [publisher, setPublisher] = useState('');
    const [plantType, setPlantType] = useState('');

    const handlePublisherChange = (publisher: string) => publisherDebounceEmit({ publisher: publisher, plantType: plantType })

    const publisherDebounceEmit = React.useCallback(
        debounce((filters) => onFilterChange(filters), 300),
        []
    );

    const setNewFilter = () => {
        onFilterChange({ publisher: publisher, plantType: plantType })
    }

    useEffect(() => {
        setNewFilter();
    }, [plantType]);

    return (
        <>
            <div className={styles.filtersContainer}>
                <div className={styles.filterInput}>
                    <Autocomplete
                        autoComplete
                        options={allPlantsNames}
                        classes={{root: styles.TextField}}
                        renderInput={(params) => 
                        <TextField variant="standard" {...params} placeholder="plant..." 
                        classes={{root: styles.input}}
                        sx={textFieldStyle}/>
                    }
                        onChange={(_event: unknown, newValue: string | null) => {
                            setPlantType(newValue ?? '');
                        }}
                    />
                </div>
                <div className={styles.filterInput}>
                    <TextField
                        className={`${styles.input} ${styles.TextField}`}
                        id="publisher"
                        placeholder="posted by..."
                        variant="standard"
                        value={publisher}
                        InputProps={{
                            startAdornment: <InputAdornment position="start">@</InputAdornment>,
                        }}
                        onChange={(e) => {
                            const { value } = e.target;
                            setPublisher(value);
                            handlePublisherChange(value);
                        }}
                        sx={textFieldStyle}
                    />
                </div>
            </div>
        </>
    )
}

export default Filters
