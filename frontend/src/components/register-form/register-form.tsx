import { ChangeEvent } from 'react';
import { Button, TextField } from '@mui/material';
import { useFormik } from 'formik';
import styles from './register-form.module.css'
import * as Yup from 'yup';
import { textFieldStyle, themeButton } from '../../common-styles';
import { UserService } from '../../services/User-service';
import { useUser } from '../../providers/User-provider';
import { useSnackbar } from '../../providers/Snackbar-provider';
import { useNavigate } from 'react-router-dom';
import { routes } from '../../routes/router';

export interface RegisterData {
    email: string,
    password: string,
    username: string,
    bio: string
    image: File | null
}

const RegisterForm = () => {
    const { setCurrUser } = useUser();
    const { openSnackbar } = useSnackbar();
    const navigate = useNavigate();

    const validationSchema = Yup.object().shape({
        username: Yup.string().required('Username is required').max(25),
        email: Yup.string().required('Email is required')
            .test('email-format', 'Invalid email format', (value) => {
                return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(value);
            }),
        password: Yup.string().required('Password is required'),
        bio: Yup.string().max(200),
        image: Yup.mixed().nullable()
    });

    const formik = useFormik({
        initialValues: {
            username: '',
            email: '',
            password: '',
            bio: '',
            image: null,
        },
        validationSchema: validationSchema,
        onSubmit: async (values: RegisterData, { resetForm }) => {
            await UserService.registerUser(values)
            .then((res) => {
                if (res.data) {
                    setCurrUser(res.data);
                    resetForm();
                    navigate(routes.homePage);
                }
            })
            .catch((error) => {
                openSnackbar(error.response.data);
            });
        },
    });

    const handleImageUpload = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.files) {
            formik.setFieldValue('image', event.target.files[0])
        }
    };

    return (
        <form className={styles.formContainer} onSubmit={formik.handleSubmit}>
            <TextField
                label="Username"
                name="username"
                value={formik.values.username}
                variant='standard'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.username && Boolean(formik.errors.username)}
                helperText={formik.touched.username && formik.errors.username}
                sx={textFieldStyle}
                required
            />
            <TextField
                label="Email"
                name="email"
                type="email"
                value={formik.values.email}
                variant='standard'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.email && Boolean(formik.errors.email)}
                helperText={formik.touched.email && formik.errors.email}
                sx={textFieldStyle}
                required
            />
            <TextField
                label="Password"
                name="password"
                type="password"
                value={formik.values.password}
                variant='standard'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.password && Boolean(formik.errors.password)}
                helperText={formik.touched.password && formik.errors.password}
                sx={textFieldStyle}
                required
            />
            <TextField
                label="Bio"
                name="bio"
                multiline
                rows={3}
                value={formik.values.bio}
                variant='standard'
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.bio && Boolean(formik.errors.bio)}
                helperText={formik.touched.bio && formik.errors.bio}
                sx={textFieldStyle}
            />
            <input
                type="file"
                name="image"
                accept="image/*"
                onChange={(event) => handleImageUpload(event)}
                onBlur={formik.handleBlur}
            />
            {formik.touched.image && formik.errors.image && (
                <div style={{ color: 'red' }}>{formik.errors.image}</div>
            )}
            <Button type="submit" variant="contained" sx={themeButton}>
                Register
            </Button>
        </form>
    );
};

export default RegisterForm;
