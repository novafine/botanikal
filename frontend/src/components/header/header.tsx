import { useNavigate } from 'react-router-dom';
import logo from '../../assets/logo.png';
import { useUser } from '../../providers/User-provider';
import { routes } from '../../routes/router';
import { UserService } from '../../services/User-service';
import ProfilePicture from '../profile-picture/ProfilePicture';
import styles from './Header.module.css';

function Header() {
    const { user, setCurrUser } = useUser();
    const navigate = useNavigate();

    const logout = async () => {
        try {
            await UserService.logoutUser(setCurrUser);
        } catch (err) {
            console.error(err);
        } finally {
            navigate(routes.login);
        }
    };

    const goToUserPage = () => navigate(`/user/${user?.username}`);

    const goToHomePage = () => navigate(routes.homePage);

    return (
        <>
            <div className={styles.headerContainer}>
                <div className={styles.clickable} onClick={goToUserPage}>
                    {user && <ProfilePicture imageUrl={UserService.getProfileImageUrl(user) ?? ''} size={50} ></ProfilePicture>}
                    {/* TODO: Add default profile pic */}
                </div>
                <div className={`${styles.logoContainer} ${styles.clickable}`} onClick={goToHomePage}>
                    BotaniKal
                    <img alt="logo" className={styles.logo} src={logo} />
                </div>
                <div>
                    {user && <p className={`${styles.logout} ${styles.clickable}`} onClick={logout}>logout</p>}
                </div>
            </div>
        </>
    );
}

export default Header;
