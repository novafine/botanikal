import axios from "axios";
import { ROUTES, getAuthHeader } from "../conf";

export interface Plant {
    name: string;
    family: string;
    discoveryYear: number;
}

const plantsMock = [
    { name: "Rose", family: "Rosaceae", discoveryYear: 1737 },
    { name: "Sunflower", family: "Asteraceae", discoveryYear: 1833 },
    { name: "Oak", family: "Fagaceae", discoveryYear: 1753 },
    { name: "Tulip", family: "Liliaceae", discoveryYear: 1594 },
    { name: "Lavender", family: "Lamiaceae", discoveryYear: 1753 },
    { name: "Palm", family: "Arecaceae", discoveryYear: 1753 },
    { name: "Daisy", family: "Asteraceae", discoveryYear: 1599 },
    { name: "Maple", family: "Sapindaceae", discoveryYear: 1753 },
    { name: "Cactus", family: "Cactaceae", discoveryYear: 1753 },
    { name: "Fern", family: "Dryopteridaceae", discoveryYear: 1753 }
];

export class PlantsService {
    static getRandomPlant = async () => {
        const randPlant = (await (axios.get<Plant>(ROUTES.GET_RANDOM_PLANT, getAuthHeader().header))).data;

        return randPlant ?? plantsMock[Math.floor(Math.random() * plantsMock.length)];
    };

    static getAllPlantsCommonNames = async () => {
        const plantsNames = (await (axios.get<Plant['name'][]>(ROUTES.GET_ALL_PLANTS_NAMES, getAuthHeader().header))).data;

        return plantsNames.length > 0 ? plantsNames : plantsMock.map(plant => plant.name);
    };
}