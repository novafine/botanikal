import axios from 'axios';
import { ROUTES, getAuthHeader } from '../conf';
import { UploadData } from '../pages/Upload-post-page/Upload-post-page';
import { PostMetadata } from '../components/post/post';
import { CommentToAdd, EditData } from '../pages/Post-page/Post-page';

export class PostService {

    static getAllPosts = async () => {
        const res = await axios.get<PostMetadata[]>(ROUTES.GET_ALL_POSTS, getAuthHeader().header);
        return res.data;
    };

    static getAllPostsOfUser = async (username: string) => {
        const res = await axios.get<PostMetadata[]>(ROUTES.GET_ALL_POSTS_OF_USER(username), getAuthHeader().header);
        return res.data;
    };

    static uploadPost = async (uploadData: UploadData) => {
        return await axios.post<PostMetadata>(ROUTES.UPLOAD_POST, uploadData,
            { headers: { 'Content-Type': 'multipart/form-data', 'Authorization': getAuthHeader().token } });
    }

    static getSinglePost = async (postId: string) => {
        const res = await axios.get<PostMetadata>(ROUTES.GET_SINGLE_POST(postId), getAuthHeader().header);
        return res.data;
    }

    static editPost = async (editData: EditData) => {
        return await axios.post<PostMetadata>(ROUTES.EDIT_POST, editData,
            { headers: { 'Content-Type': 'multipart/form-data', 'Authorization': getAuthHeader().token } })
    }

    static deletePost = async (postId: string) => {
        return await axios.delete(ROUTES.DELETE_POST(postId), getAuthHeader().header);
    }

    static addComment = async (comment: CommentToAdd) => {
        return await axios.post<PostMetadata>(ROUTES.ADD_COMMENT, comment, getAuthHeader().header);
    }
}