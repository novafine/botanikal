import axios from "axios";
import { LoginData } from "../components/login-form/Login-form";
import { BACKEND_URL, ROUTES, getAuthHeader, getRefreshTokenFromStorage } from "../conf";
import { RegisterData } from "../components/register-form/register-form";
import { CredentialResponse } from "@react-oauth/google";
import { EditUserData } from "../pages/User-page/UserPage";

export interface User {
    _id: string;
    email: string,
    username: string,
    bio: string,
    imgUrl: string;
    refreshToken: string;
    accessToken: string;
}

export class UserService {
    static getUserByUsername = async (username: string) => {
        const res = await axios.get<User>(ROUTES.GET_USER_BY_USERNAME(username), getAuthHeader().header);
        return res.data;
    };

    static getProfileImageUrl = (user: User | undefined): string => user?.imgUrl ? `${BACKEND_URL}/${user?.imgUrl}` : 'someDefaultPathToStaticPic';

    static loginUser = async (loginDetails: LoginData) => {
        return await axios.post<User>(ROUTES.LOGIN_USER, loginDetails);
    };

    static registerUser = async (registerDetails: RegisterData) => {
        return await axios.post(ROUTES.REGISTER_USER, registerDetails, { headers: { 'Content-Type': 'multipart/form-data' } });
    };

    static logoutUser = async (setCurrUser: (userData: User | null) => void) => {
        setCurrUser(null);
        localStorage.removeItem('token');
        localStorage.removeItem('refreshToken')

        return await axios.post(ROUTES.LOGOUT_USER, {}, getAuthHeader().header);
    };

    static updateUser = (updateUserParams: EditUserData) => axios.post(ROUTES.UPDATE_USER, updateUserParams,
        { headers: { 'Content-Type': 'multipart/form-data', 'Authorization': getAuthHeader().token } });

    static loginWithGoogle = async (credentialResponse: CredentialResponse) => {
        return await axios.post(ROUTES.GOOGLE_LOGIN, credentialResponse);
    };

    static getUser = async (username: string) => {
        const res = await axios.get(ROUTES.GET_USER(username), getAuthHeader().header);
        return res.data;
    };

    static refreshAccessToken = async () => {
        const res = await axios.get<User>(ROUTES.REFRESH_TOKENS, { headers: { 'Authorization': `jwt ${getRefreshTokenFromStorage()}` } });
        return res.data
    }
}
