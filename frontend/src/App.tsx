import { GoogleOAuthProvider } from '@react-oauth/google';
import './App.css';
import Header from './components/header/header';
import { SnackbarProvider } from './providers/Snackbar-provider';
import { UserProvider } from './providers/User-provider';
import Router from './routes/router';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <>
      <BrowserRouter>
        <GoogleOAuthProvider clientId="985313690997-dthumjnhujn8vbaj68msctdp8thkkc9e.apps.googleusercontent.com">
          <UserProvider>
            <SnackbarProvider>
              <div className='app-container'>
                <div className='header'>
                  <Header></Header>
                </div>
                <div className='content'>
                  <Router />
                </div>
              </div>
            </SnackbarProvider>
          </UserProvider>
        </GoogleOAuthProvider>
      </BrowserRouter>
    </>
  );
}

export default App;