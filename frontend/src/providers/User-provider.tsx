import { ReactNode, createContext, useContext, useEffect, useState } from "react";
import { User, UserService } from "../services/User-service";
import { getTokenFromStorage } from "../conf";

interface UserContext {
  user?: User;
  setCurrUser: (userData: User | null) => void;
}

export const UserContext = createContext<UserContext>({
  setCurrUser: () => { }
});

export const UserProvider = ({ children }: { children: ReactNode; }) => {
  const [user, setUser] = useState<User>();
  const [isLoading, setIsLoading] = useState<boolean>(true);

  useEffect(() => {
    getUserToken();
  }, []);

  const getUserToken = async () => {
    const accessToken = getTokenFromStorage();
    if (accessToken) {
      try {
        const user = await UserService.refreshAccessToken();
        if (user) {
          setCurrUser(user);
        }
      } catch (error) {
        localStorage.removeItem('refreshToken');
        localStorage.removeItem('token');
      }
    }
    setIsLoading(false);
  };

  const setCurrUser = (userData: User | null) => {
    if (userData) {
      setUser(userData);
      if (userData.accessToken) {
        localStorage.setItem('token', userData.accessToken);
      }
      if (userData.refreshToken) {
        localStorage.setItem('refreshToken', userData.refreshToken);
      }
    } else {
      setUser(undefined);
    }
  };

  if (isLoading) return <></>;

  return (
    <UserContext.Provider value={{ user, setCurrUser }}>
      {children}
    </UserContext.Provider>
  );
};

export const useUser = () => useContext(UserContext);