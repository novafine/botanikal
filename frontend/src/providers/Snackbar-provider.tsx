import { Snackbar } from '@mui/material';
import { ReactNode, createContext, useContext, useState } from 'react';

interface SnackbarContext {
    openSnackbar: (message: string) => void
}

const snackbarContext = createContext<SnackbarContext>({
    openSnackbar: () => { }
});

export const useSnackbar = () => useContext(snackbarContext);

export const SnackbarProvider = ({ children }: { children: ReactNode }) => {
    const [snackbarOpen, setSnackbarOpen] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState('');

    const openSnackbar = (message: string) => {
        setSnackbarMessage(message);
        setSnackbarOpen(true);
    };

    const closeSnackbar = () => {
        setSnackbarOpen(false);
    };

    return (
        <snackbarContext.Provider value={{ openSnackbar }}>
            {children}
            <Snackbar
                autoHideDuration={5000}
                open={snackbarOpen}
                message={snackbarMessage}
                onClose={closeSnackbar}
            />
        </snackbarContext.Provider>
    );
};
