export const textFieldStyle = {
    "& label.Mui-focused": {
        color: 'var(--color-lightBrown)'
    },
    "& .MuiInput-underline": {
        borderBottomColor: 'var(--color-lightBrown)'
    },
    "& .MuiInput-underline:after": {
        borderBottomColor: 'var(--color-lightBrown)'
    },
    "& .MuiInputLabel-root": {
        color: 'var(--color-lightBrown)'
    }
}

export const themeButton = {
        color: 'var(--color-lightPink)',
        backgroundColor: 'var(--color-darkGreen)',

        '&:hover': {
            color: 'var(--color-lightPink)',
            backgroundColor: 'var(--color-darkGreen)',
    
          }
}