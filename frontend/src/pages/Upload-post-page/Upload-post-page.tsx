import { TextField, Button, Autocomplete, IconButton } from "@mui/material";
import { useFormik } from "formik";
import { ChangeEvent, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { textFieldStyle, themeButton } from "../../common-styles";
import { useUser } from "../../providers/User-provider";
import * as Yup from 'yup';
import { PlantsService } from "../../services/Plant-service";
import styles from './Upload-post-page.module.css'
import { CheckOutlined, FileUploadOutlined } from "@mui/icons-material";
import { User } from "../../services/User-service";
import { PostService } from "../../services/Post-service";
import { useSnackbar } from "../../providers/Snackbar-provider";
import { routes } from "../../routes/router";

interface UploadFormProps {
    plantType: string,
    description: string,
    image: File | null
}

export interface UploadData extends UploadFormProps {
    image: File,
    publisher: string
}

function UploadPostPage() {

    const { user } = useUser();
    const { openSnackbar } = useSnackbar();
    const navigate = useNavigate();
    const [allPlantsNames, setAllPlantsNames] = useState([""]);
    const [imagePreview, setImagePreview] = useState('');


    const validationSchema = Yup.object().shape({
        plantType: Yup.string().required(),
        description: Yup.string().optional(),
        image: Yup.mixed().required()
    });

    const formik = useFormik({
        initialValues: {
            plantType: '',
            description: '',
            image: null
        },
        validationSchema: validationSchema,
        onSubmit: async (values: UploadFormProps, { resetForm }) => {
            const uploadData: UploadData = {
                ...values,
                image: values.image as File,
                publisher: (user as User).username
            }
            await PostService.uploadPost(uploadData).then((res) => {
                if (res.data) {
                    resetForm();
                    navigate(routes.homePage);
                }
            })
                .catch((error) => {
                    openSnackbar(error.response.data);
                });

        },
    });

    const handleImageUpload = (event: ChangeEvent<HTMLInputElement>) => {
        const file = event.target?.files ? event.target?.files[0] : null
        formik.setFieldValue('image', file);
        if (file) {
            const reader = new FileReader();
            reader.onloadend = () => {
                setImagePreview(reader.result as string);
            };
            reader.readAsDataURL(file);
        } else {
            setImagePreview('');
        }
    };

    useEffect(() => {
        const getAllPlantsCommonNames = async () => {
            const allPlantNames = await PlantsService.getAllPlantsCommonNames();
            setAllPlantsNames(allPlantNames);
        }
        getAllPlantsCommonNames();
    }, []);

    return (
        <>
            <div className={styles.uploadContainer}>
                <form className={styles.formContainer} onSubmit={formik.handleSubmit}>
                    <div className={styles.imageContainer}>
                        <input
                            id="image-upload"
                            type="file"
                            accept="image/*"
                            onChange={handleImageUpload}
                            style={{ display: 'none' }}
                        />

                        {formik.touched.image && formik.errors.image && (
                            <div style={{ color: 'red' }}>{formik.errors.image}</div>
                        )}
                        {!imagePreview ? (
                            <label htmlFor="image-upload">
                                <IconButton component="span" sx={{
                                    color: 'var(--color-darkGreen)',
                                    scale: '2'
                                }}>
                                    <FileUploadOutlined />
                                </IconButton>
                            </label>
                        ) : (
                            <img src={imagePreview} alt="Image Preview" className={styles.imagePreview}
                                onClick={() => {
                                    const fileInput = document.getElementById('image-upload');
                                    if (fileInput) {
                                        fileInput.click();
                                    }
                                }}
                            />
                        )}
                    </div>
                    <div className={styles.inputsContainer}>
                        <Autocomplete
                            autoComplete
                            options={allPlantsNames}
                            value={formik.values.plantType}
                            onChange={(_event, newValue) => {
                                formik.setFieldValue('plantType', newValue ? newValue : '');
                            }}
                            renderInput={(params) => (
                                <TextField
                                    classes={{ root: styles.input }}
                                    variant="standard"
                                    {...params}
                                    placeholder="plant..."
                                    sx={textFieldStyle}
                                    name="plantType"
                                    label="Plant type"
                                    error={formik.touched.plantType && Boolean(formik.errors.plantType)}
                                    helperText={formik.touched.plantType && formik.errors.plantType}
                                    required
                                    margin="normal"
                                />
                            )}
                        />
                        <TextField
                            className={styles.input}
                            name="description"
                            label="Description"
                            value={formik.values.description}
                            onChange={formik.handleChange}
                            multiline
                            rows={4}
                            margin="normal"
                        />
                        <Button type="submit" variant="contained" sx={{
                            ...themeButton,
                            borderRadius: '50%',
                            height: '64px',
                            alignSelf: 'end',
                            marginTop: '40px'
                        }}>
                            <CheckOutlined />
                        </Button>
                    </div>
                </form>
            </div>
        </>
    )
}

export default UploadPostPage
