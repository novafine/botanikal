import DoneIcon from '@mui/icons-material/Done';
import ModeEditOutlineOutlinedIcon from '@mui/icons-material/ModeEditOutlineOutlined';
import TextField from '@mui/material/TextField';
import { ChangeEvent, useEffect, useState } from "react";
import Gallery from "../../components/Gallery/Gallery";
import { PostMetadata } from '../../components/post/post';
import ProfilePicture from "../../components/profile-picture/ProfilePicture";
import { useUser } from '../../providers/User-provider';
import { PostService } from "../../services/Post-service";
import { UserService } from "../../services/User-service";
import styles from "./UserPage.module.css";
import { useSnackbar } from '../../providers/Snackbar-provider';

export interface EditUserData {
    bio?: string;
    image?: File;
}

const UserPage = () => {
    const { openSnackbar } = useSnackbar();
    const [posts, setPosts] = useState<PostMetadata[]>([]);
    const [isEditBioMode, setIsEditBioMode] = useState<boolean>(false);
    const { user, setCurrUser } = useUser();
    const EMPTY_BIO = '<write something about yourself!>';
    const [userBio, setUserBio] = useState(user?.bio || EMPTY_BIO);

    useEffect(() => {
        const getAllPostsOfUser = async (username: string) => {
            const userPosts = await PostService.getAllPostsOfUser(username);
            setPosts(userPosts);
        };

        if (user)
            getAllPostsOfUser(user.username);
    }, [user]);

    const handleImageUpload = async (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.files)
            editUser({ image: event.target.files[0] });
    };

    const handleEditBio = () => {
        editUser({ bio: userBio });
    };

    const editUser = async (editData: EditUserData) => {
        await UserService.updateUser(editData).then((res) => {
            if (res.data) {
                setCurrUser(res.data);
                setIsEditBioMode(false);
            }
        })
            .catch((error) => {
                openSnackbar(error.response.data);
            });
    };

    return (<>
        <div className={styles.userPageContainer}>
            <div className={styles.itemWithEditContainer}>
                <div className={styles.profilePictureWithUsername}>
                    <ProfilePicture imageUrl={UserService.getProfileImageUrl(user)} size={150}></ProfilePicture>
                    <p className={styles.username}>@{user?.username}</p>
                </div>
                <input
                    style={{
                        display: "none"
                    }}
                    accept="*"
                    id="choose-file"
                    type="file"
                    onChange={handleImageUpload}
                />
                <label className={styles.clickable} htmlFor="choose-file">
                    <ModeEditOutlineOutlinedIcon></ModeEditOutlineOutlinedIcon>
                </label>
            </div>
            <div className={styles.bio}>
                {isEditBioMode ?
                    <form className={styles.itemWithEditContainer}>
                        <TextField
                            id="standard-basic"
                            variant="standard"
                            fullWidth={true}
                            value={userBio}
                            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                setUserBio(event.target.value);
                            }}
                            inputProps={{ size: userBio.length, maxLength: 200 }} />
                        <div className={styles.clickable} onClick={handleEditBio}>
                            <DoneIcon></DoneIcon>
                        </div>
                    </form>
                    :
                    <div className={styles.itemWithEditContainer}>
                        <p>{userBio || EMPTY_BIO}</p>
                        <div className={styles.clickable} onClick={() => setIsEditBioMode(true)}>
                            <ModeEditOutlineOutlinedIcon ></ModeEditOutlineOutlinedIcon>
                        </div>
                    </div>
                }
            </div>
            <div className={styles.postsContainer}>
                <Gallery posts={posts} />
            </div>
        </div >
    </>
    );
};

export default UserPage;