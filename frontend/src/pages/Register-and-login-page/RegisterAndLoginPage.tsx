import styles from './RegisterAndLoginPage.module.css'
import LoginForm from "../../components/login-form/Login-form";
import { Divider } from "@mui/material";
import RegisterForm from "../../components/register-form/register-form";
import { UserService } from '../../services/User-service';
import { useUser } from '../../providers/User-provider';
import { useSnackbar } from '../../providers/Snackbar-provider';
import { useNavigate } from 'react-router-dom';
import { routes } from '../../routes/router';
import { CredentialResponse, GoogleLogin } from '@react-oauth/google'



function RegisterAndLoginPage() {
    const { setCurrUser } = useUser();
    const { openSnackbar } = useSnackbar();
    const navigate = useNavigate();

    const signWithGoogle = async (credentialResponse: CredentialResponse) => {
        await UserService.loginWithGoogle(credentialResponse).then((res) => {
            if (res.data) {
                setCurrUser(res.data);
                navigate(routes.homePage);
            }
        })
            .catch((error) => {
                openSnackbar(error.response.data);
            });
    }

    return (
        <>
            <div className={styles.container}>
                <div className={styles.loginAndRegisterContainer}>
                    <RegisterForm></RegisterForm>
                    <Divider className={styles.devider} variant="middle" orientation="vertical" flexItem />
                    <div className={`${styles.loginContainer} ${styles.inputsContainer}`}>
                        <LoginForm></LoginForm>
                    </div>
                </div>
                <Divider className={styles.devider} variant="middle" flexItem />
                <GoogleLogin onSuccess={signWithGoogle} onError={() => openSnackbar('Failed to login')} locale="en" />
            </div>
        </>
    );
}

export default RegisterAndLoginPage
