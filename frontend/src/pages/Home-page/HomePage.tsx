import { useEffect, useState } from "react";
import Gallery from "../../components/Gallery/Gallery";
import Filters, { FilterOptions } from "../../components/filters/Filters";
import { PostMetadata } from "../../components/post/post";
import UploadPostBtn from "../../components/upload-post-btn/upload-post-btn";
import { PlantsService } from "../../services/Plant-service";
import { PostService } from "../../services/Post-service";
import styles from './HomePage.module.css';

function HomePage() {

    const [plantInfo, setPlantInfo] = useState("");
    const [allPlantsNames, setAllPlantsNames] = useState([""]);
    const [posts, setPosts] = useState<PostMetadata[]>([]);
    const [filterType, setFilterType] = useState<FilterOptions>({
        publisher: '',
        plantType: '',
    })

    const setNewFilter = (newFilter: FilterOptions) => {
        return posts.filter((post: PostMetadata) => newFilter.publisher === '' || post.publisher.includes(newFilter.publisher))
            .filter(post => newFilter.plantType === '' || post.plantType === newFilter.plantType);
    }

    useEffect(() => {
        const getPlantInfo = async () => {
            const { name, family, discoveryYear } = await PlantsService.getRandomPlant();

            const plantInfo = `The ${name} is from the family of ${family} and was first discovered in ${discoveryYear}`;
            setPlantInfo(plantInfo);
        };

        const getAllPlantsCommonNames = async () => {
            const allPlantNames = await PlantsService.getAllPlantsCommonNames();
            setAllPlantsNames(allPlantNames);
        };

        const getAllPosts = async () => {
            const allPosts = await PostService.getAllPosts();
            setPosts(allPosts);
            // setFilteredPosts(allPosts);
        };

        getPlantInfo();
        getAllPlantsCommonNames();
        getAllPosts();
    }, []);



    return (
        <>
            <div className={styles.homePageContainer}>
                <div className={styles.funFactContainer}>
                    <h1>Fun fact!</h1>
                    <span className={styles.funFactInfo}>{plantInfo}</span>
                </div>
                <div className={styles.filtersContainer}>
                    <Filters
                        allPlantsNames={allPlantsNames}
                        onFilterChange={setFilterType}
                    />
                </div>
                <div className={styles.postsContainer}>
                    <Gallery posts={setNewFilter(filterType)} />
                </div>
                <UploadPostBtn></UploadPostBtn>
            </div>
        </>
    );
}

export default HomePage;
