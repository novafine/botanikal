import { ChangeEvent, useEffect, useState } from "react"
import { Comment, PostMetadata } from "../../components/post/post"
import { useNavigate, useParams } from "react-router-dom";
import { PostService } from "../../services/Post-service";
import styles from './Post-page.module.css'
import { User, UserService } from "../../services/User-service";
import { CheckOutlined, CloseOutlined, DeleteOutline, EditOutlined, ModeCommentOutlined, SendOutlined } from "@mui/icons-material";
import { useUser } from "../../providers/User-provider";
import { Divider, IconButton, TextField } from "@mui/material";
import { BACKEND_URL } from "../../conf";
import { useSnackbar } from "../../providers/Snackbar-provider";
import { routes } from "../../routes/router";

export interface EditData {
    postId: string;
    description?: string;
    image?: File
}

export interface CommentToAdd {
    postId: string
    commenter: string;
    description: string;
}

function PostPage() {

    const { postId } = useParams();
    const { user } = useUser();
    const navigate = useNavigate();
    const { openSnackbar } = useSnackbar();
    const [post, setPost] = useState<PostMetadata>({
        _id: '',
        publisher: '',
        comments: [],
        imagePath: '',
        plantType: '',
        description: ''
    });
    const [publisher, setPublisher] = useState<User>();
    const [isEditing, setIsEditing] = useState(false);
    const [editedDescription, setEditedDescription] = useState('');
    const [newComment, setNewComment] = useState('');

    const isCurrUserPost = () => user?.username === publisher?.username;

    const handleEditClick = () => {
        setIsEditing(true);
    };

    const handleCancelEdit = () => {
        setIsEditing(false);
        setEditedDescription((post as PostMetadata).description);
    };

    const handleSaveEdit = async () => {
        const editData: EditData = {
            postId: (post as PostMetadata)._id,
            description: editedDescription
        }
        await editPost(editData);
    };

    const editPost = async (editData: EditData) => {
        await PostService.editPost(editData).then((res) => {
            if (res.data) {
                setPost(res.data);
                setIsEditing(false);
            }
        })
            .catch((error) => {
                openSnackbar(error.response.data);
            });
    }

    const handleDescriptionChange = (event: any) => {
        setEditedDescription(event.target.value);
    };

    const handleDeletePost = async () => {
        await PostService.deletePost((post as PostMetadata)._id).then(() => {
            navigate(routes.homePage);
        })
            .catch((error) => {
                openSnackbar(error.response.data);
            });
    }

    const handleImageChange = async (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target?.files && event.target?.files[0]) {
            const editData = {
                postId: (post as PostMetadata)._id,
                image: event.target?.files[0]
            }
            await editPost(editData)
        }
    };

    const handleImageClick = () => {
        const fileInput = document.getElementById('image-upload');
        if (fileInput) {
            fileInput.click();
        }
    };

    const handleNewCommentChange = (event: any) => {
        setNewComment(event.target.value);
    }

    const addNewComment = async () => {
        if (newComment.length > 0) {
            const commentData: CommentToAdd = {
                postId: post._id,
                commenter: (user as User).username,
                description: newComment
            }

            await PostService.addComment(commentData).then((res) => {
                if (res.data) {
                    setPost(res.data);
                    setNewComment('');
                }
            })
                .catch((error) => {
                    openSnackbar(error.response.data);
                });
        }
    }

    useEffect(() => {

        const getPost = async () => {
            if (postId) {
                const post = await PostService.getSinglePost(postId);
                setPost(post);
                getPublisher(post.publisher);
                setEditedDescription(post.description)
            }
        }

        const getPublisher = async (username: string) => {
            const publisher = await UserService.getUser(username);
            setPublisher(publisher);
        }

        getPost();
    }, []);

    return (
        <>
            <div className={styles.postPageContainer}>
                <div className={styles.imageContainer}>
                    <img
                        src={`${BACKEND_URL}/${post.imagePath}`}
                        onClick={isCurrUserPost() ? handleImageClick : undefined}
                    />
                </div>
                <input
                    id="image-upload"
                    type="file"
                    accept="image/*"
                    style={{ display: 'none' }}
                    onChange={handleImageChange}
                />
                <div className={styles.dataContainer}>
                    <div className={styles.publisherData}>
                        <div className={styles.profileInfo}>
                            <img className={styles.profilePic} src={`${BACKEND_URL}/${publisher?.imgUrl}`}></img>
                            <span className={styles.username}>@{publisher?.username}</span>
                        </div>
                        <span>{post.comments.length} <ModeCommentOutlined /></span>
                        <div>
                            {isCurrUserPost() &&
                                (
                                    <div>
                                        {isEditing ? (
                                            <>
                                                <IconButton onClick={handleSaveEdit}>
                                                    <CheckOutlined />
                                                </IconButton>
                                                <IconButton onClick={handleCancelEdit}>
                                                    <CloseOutlined />
                                                </IconButton>
                                            </>
                                        ) : (
                                            <>
                                                <IconButton onClick={handleEditClick}>
                                                    <EditOutlined />
                                                </IconButton>
                                                <IconButton onClick={handleDeletePost}>
                                                    <DeleteOutline />
                                                </IconButton>
                                            </>
                                        )}
                                    </div>

                                )}
                        </div>
                    </div>
                    <div className={styles.descriptionContainer}>
                        {isEditing ? (
                            <TextField
                                value={editedDescription}
                                onChange={handleDescriptionChange}
                                multiline
                                fullWidth
                            />
                        ) : (
                            <div>{post.description}</div>
                        )}
                    </div>
                    <div className={styles.commentsContainer}>
                        <div className={styles.addComment}>
                            <TextField
                                value={newComment}
                                onChange={handleNewCommentChange}
                                multiline
                                maxRows={3}
                                sx={{
                                    flexGrow: 2,
                                }}
                            />
                            <IconButton onClick={addNewComment}>
                                <SendOutlined />
                            </IconButton>
                        </div>
                        <div className={styles.commentsList}>
                            {post.comments.map((comment: Comment, index: number) => (
                                <div key={index} className={styles.commentContainer}>
                                    <span className={styles.commenter}>@{comment.commenter}</span>
                                    <span className={styles.description}>{comment.description}</span>
                                    <Divider />
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div >
        </>
    )
}

export default PostPage
