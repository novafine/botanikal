import { Navigate, Outlet } from "react-router-dom";
import { routes } from "./router";
import { useUser } from "../providers/User-provider";

const PrivateRoute = () => {
    const {user} = useUser();
    
    return user ? <Outlet /> : <Navigate to={routes.login} />;
};

export default PrivateRoute;