import { Navigate, Route, Routes } from "react-router-dom";
import HomePage from "../pages/Home-page/HomePage";
import UserPage from "../pages/User-page/UserPage";
import PrivateRoute from "./PrivateRoute";
import RegisterAndLoginPage from "../pages/Register-and-login-page/RegisterAndLoginPage";
import UploadPostPage from "../pages/Upload-post-page/Upload-post-page";
import PostPage from "../pages/Post-page/Post-page";

export const routes = {
    login: '/login',
    homePage: '/',
    userPage: '/user/:username',
    uploadPostPage: '/upload',
    postPage: '/post/:postId'
};

const Router = () => (<>
        <Routes>
            <Route path={routes.login} element={<RegisterAndLoginPage />} />
            <Route element={<PrivateRoute />}>
                <Route path={routes.homePage} element={<HomePage />} />
                <Route path={routes.uploadPostPage} element={<UploadPostPage />} />
                <Route path={routes.postPage} element={<PostPage />} />
                <Route path={routes.userPage} element={<UserPage />} />
                <Route path="*" element={<Navigate to={routes.homePage} />} />
            </Route>
        </Routes>
</>);

export default Router;