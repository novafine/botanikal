import Joi from "joi";

export interface User extends Omit<UserRegistrationParams, "image"> {
    _id: string;
    imgUrl: string
    refreshTokens: string[];
}

export interface UserRegistrationParams {
    email: string;
    password: string;
    username: string;
    image?: File;
    bio?: string;
}

export interface UserLoginParams {
    email: string;
    password: string;
}

export type UserWithoutId = Omit<User, "_id">

export interface EditUserParams {
    imgUrl?: string;
    bio?: string;
}

export const EditUserSchema = Joi.object({
    bio: Joi.string().optional().allow(''),
    image: Joi.any().meta({ swaggerType: 'file' }).optional().allow(null)
});

export const RegisterSchema = Joi.object<UserRegistrationParams>({
    email: Joi.string().email().required(),
    username: Joi.string().max(25).required(),
    password: Joi.string().required(),
    bio: Joi.string().max(200).optional().allow(''),
    image: Joi.any().meta({ swaggerType: 'file' }).optional().allow(null)
})

export const LoginSchema = Joi.object<UserRegistrationParams>({
    email: Joi.string().email().required(),
    password: Joi.string().required()
})