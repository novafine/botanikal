import { Request } from 'express';

export interface TokenInfo {
    _id: string;
}
export interface AuthRequest extends Request {
    user?: { _id: string; };
}