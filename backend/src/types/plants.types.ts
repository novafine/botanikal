export interface Plant {
    name: string;
    family: string;
    discoveryYear: number
}