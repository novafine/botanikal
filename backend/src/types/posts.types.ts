import Joi from "joi";

export interface Post extends Omit<UploadPostData, 'image'> {
    comments: Comment[];
    imagePath: string;
}

export interface Comment {
    commenter: string;
    description: string;
}

export interface UploadPostData {
    plantType: string,
    description: string,
    image: File,
    publisher: string
}

export interface EditPostData {
    postId: string,
    description?: string,
    image?: File 
}

export interface EditPostParams {
    postId: string,
    fieldsToEdit: {
        description? :string
        imagePath?: string 
    }
}

export interface CommentToAdd {
    postId: string
    commenter: string;
    description: string;
}

export const UploadPostSchema = Joi.object<UploadPostData>({
    plantType: Joi.string().required(),
    description: Joi.string().allow(''),
    publisher: Joi.string().required(),
})

export const EditPostSchema = Joi.object<EditPostData>({
    postId: Joi.string().required(),
    description: Joi.string().optional().allow('').allow(null),
    image: Joi.any().meta({ swaggerType: 'file' }).optional().allow(null)
})

export const CommentToAddSchema = Joi.object<CommentToAdd>({
    postId: Joi.string().required(),
    description: Joi.string().required(),
    commenter: Joi.string().required()
})