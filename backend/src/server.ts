import bodyParser from "body-parser";
import cors from "cors";
import dotenv from 'dotenv';
import express from "express";
import { Express } from "express-serve-static-core";
import * as fs from 'fs';
import swaggerJsDoc from "swagger-jsdoc";
import swaggerUI from "swagger-ui-express";
import { authMiddleware } from "./middlewares/auth.middlewares";
import { connectToMongo } from "./mongo-connection";
import { createAuthRouter } from "./routes/auth.routes";
import { createPlantsRouter } from "./routes/plants.routes";
import { createPostsRouter } from "./routes/posts.routes";
import { createUserRouter } from "./routes/user.routes";
import { PlantsPoller } from "./trefle.api";

export const initServer = async (): Promise<Express> => {
    if (process.env.NODE_ENV === 'test')
        dotenv.config({ path: './.test.env' });
    else if (process.env.NODE_ENV === 'production')
        dotenv.config({ path: './.prod.env' });
    else
        dotenv.config();

    await connectToMongo();
    const app = express();

    if (!fs.existsSync(process.env.UPLOADS_PATH))
        fs.mkdirSync(process.env.UPLOADS_PATH);

    app.use(cors());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(express.static('public'));
    const options = {
        definition: {
            openapi: '3.0.0',
            info: {
                title: 'Botanikal REST API',
                version: '1.0.0',
                description: 'Botanikal REST server including authentication using JWT',
            },
            servers: [
                {
                    url: 'http://localhost:4000'
                },
                {
                    url: 'https://node59.cs.colman.ac.il:4000'
                }
            ]
        },
        apis: ['./src/routes/*.ts']
    };
    const specs = swaggerJsDoc(options);
    app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(specs));
    app.use('/auth', createAuthRouter());
    app.use('/posts', authMiddleware, createPostsRouter());
    app.use('/user', authMiddleware, createUserRouter());
    app.use('/plants', authMiddleware, createPlantsRouter());
    app.use(`/${process.env.UPLOADS_PATH}`, express.static(process.env.UPLOADS_PATH));

    if (process.env.NODE_ENV !== 'test')
        PlantsPoller.startPoller();

    return app;
};