import { Schema } from "joi";

export const validate = <T>(object: T, schema: Schema): T => {
    const { error, value } = schema.validate(object);
    if (error) {
        throw new Error(`Validation error: ${error.message}`);
    }
    return value as T;
};