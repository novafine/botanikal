import { initServer } from "./server";
import http from "http";
import https from "https";
import fs from "fs";

const initApp = async () => {
    const app = await initServer();

    if (process.env.NODE_ENV !== 'production') {
        console.log('development');
        http.createServer(app).listen(process.env.PORT, () => {
            console.log(`Server running http app on port ${process.env.PORT}`);
        });
    } else {
        console.log('production');
        const options = {
            key: fs.readFileSync('./client-key2.pem'),
            cert: fs.readFileSync('./client-cert2.pem'),
        };
        https.createServer(options, app).listen(process.env.PORT);
    }
};

initApp();