import { DEFAULT_USER_IMG_URL } from "../controllers/auth.controller";
import { EditUserParams, UserLoginParams, UserRegistrationParams, UserWithoutId } from "../types/users.types";

export const userToLogin: UserLoginParams = {
    email: "testUser@test.com",
    password: "1234567890",
}

export const userToRegister: UserRegistrationParams = {
    ...userToLogin,
    username: 'user1',
    bio: ''
};

export const basicUserMock: UserWithoutId = {
    ...userToRegister,
    imgUrl: DEFAULT_USER_IMG_URL,
    refreshTokens: []
};

export const basicUserMock2: UserWithoutId = {
    email: "testUser2@test.com",
    password: "1234567890",
    username: 'user2',
    imgUrl: DEFAULT_USER_IMG_URL,
    refreshTokens: []
};

export const userWithFullDetailsMock: UserWithoutId = {
    email: "testUser3@test.com",
    password: "1234567890",
    username: 'user3',
    imgUrl: '/src/img.png',
    bio: 'hello world',
    refreshTokens: []
};

export const editMock: EditUserParams = {
    bio: 'new bio',
};