import { Express } from "express-serve-static-core";
import { StatusCodes } from "http-status-codes";
import mongoose from "mongoose";
import request from 'supertest';
import { PlantModel } from "../models/plants.model";
import { initServer } from "../server";
import { updateNewPlants } from "../services/plants.service";
import { PLANTS_MOCK } from "./plants.mocks";
import { userToRegister } from "./users.mock";
import { UserModel } from "../models/users.model";

describe('plants router', () => {
    let server: Express;
    let accessToken: string;

    beforeAll(async () => {
        server = await initServer();
        await UserModel.deleteMany();
        accessToken = (await request(server).post("/auth/register").send(userToRegister)).body.accessToken;
    });

    beforeEach(async () => {
        await mongoose.connect(process.env.DB_URL);
        await PlantModel.deleteMany();
        await UserModel.deleteMany();
        await updateNewPlants(PLANTS_MOCK);
    });

    afterEach(async () =>
        await mongoose.connect(process.env.DB_URL)
    );

    afterAll(async () => {
        await PlantModel.deleteMany();
        await mongoose.connection.close();
    });

    test('GET /plants/plantsNames/', async () => {
        const res = await request(server).get('/plants/plantsNames').set("Authorization", "JWT " + accessToken);
        expect(res.statusCode).toEqual(StatusCodes.OK);
        expect(JSON.parse(res.text)).toEqual(PLANTS_MOCK.map(plant => plant.name));
    });

    test('GET /plants/randomPlant', async () => {
        const res = await request(server).get(`/plants/randomPlant`).set("Authorization", "JWT " + accessToken);
        expect(res.statusCode).toEqual(StatusCodes.OK);
        const { _id, ...randomPlant } = JSON.parse(res.text);
        expect(PLANTS_MOCK).toEqual(expect.arrayContaining([randomPlant]));
    });
});