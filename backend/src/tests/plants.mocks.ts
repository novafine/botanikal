import { Plant } from "../types/plants.types";

export const PLANTS_MOCK: Plant[] = [
    {
        name: 'Apple',
        family: 'Apple',
        discoveryYear: 2019
    },
    {
        name: 'Banana',
        family: 'Banana',
        discoveryYear: 2020
    },
    {
        name: 'Orange',
        family: 'Orange',
        discoveryYear: 1758
    },
    {
        name: 'Pineapple',
        family: 'Pineapple',
        discoveryYear: 1965
    },
    {
        name: 'Strawberry',
        family: 'Strawberry',
        discoveryYear: 2000
    },
    {
        name: 'Watermelon',
        family: 'Watermelon',
        discoveryYear: 1887
    },
    {
        name: 'Mango',
        family: 'Mango',
        discoveryYear: 1910
    },
];