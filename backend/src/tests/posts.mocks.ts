import { Readable } from "stream";
import { AuthRequest } from "../types/auth.types";
import { EditPostParams, Post, UploadPostData } from "../types/posts.types";
import { basicUserMock, userToRegister } from "./users.mock";

export const POST_MOCK: Post = {
    plantType: 'apple',
    description: 'test',
    publisher: basicUserMock.username,
    comments: [],
    imagePath: 'its a stam path'
};

export const UPLOAD_POST_REQUEST_MOCK: Omit<UploadPostData, 'image'> = {
    publisher: userToRegister.username,
    description: "",
    plantType: 'oak'
};