import { Express } from "express-serve-static-core";
import { StatusCodes } from "http-status-codes";
import mongoose from "mongoose";
import request from 'supertest';
import { initServer } from "../server";
import { basicUserMock, basicUserMock2, editMock, userToRegister } from "./users.mock";
import { UserModel } from "../models/users.model";

let server: Express;
let accessToken: string;
const IMAGE_PATH = 'public/userDefault.jpg';

beforeAll(async () => {
    server = await initServer();
    await UserModel.deleteMany();
});

afterAll(async () => {
    await mongoose.connect(process.env.DB_URL);
    await UserModel.deleteMany();
    await mongoose.connection.close();
});

describe('user router', () => {
    beforeEach(async () => {
        await mongoose.connect(process.env.DB_URL);
        await UserModel.deleteMany();
    });

    afterEach(async () =>
        await mongoose.connect(process.env.DB_URL)
    );

    afterAll(async () => {
        await mongoose.connect(process.env.DB_URL)
        await UserModel.deleteMany();
        await mongoose.connection.close();
    });

    describe(('get user by username'), () => {

        beforeAll(async () => {
            await mongoose.connect(process.env.DB_URL);
            accessToken = (await request(server).post("/auth/register").send(userToRegister)).body.accessToken;
        })

        test('succeed get user by username', async () => {
            await UserModel.create(basicUserMock);
            const res = await request(server).get(`/user/${basicUserMock.username}`).set("Authorization", "JWT " + accessToken);
            expect(res.statusCode).toBe(StatusCodes.OK);
            expect(res.body.email).toEqual(basicUserMock.email);
            expect(res.body.username).toEqual(basicUserMock.username);
            expect(res.body.bio).toEqual(basicUserMock.bio);
            expect(res.body.imgUrl).toEqual(basicUserMock.imgUrl);
        })

        test('user not found', async () => {
            const res = await request(server).get(`/user/${basicUserMock2.username}`).set("Authorization", "JWT " + accessToken);
            expect(res.statusCode).toBe(StatusCodes.NOT_FOUND);
        })

        test('failed get user by username', async () => {
            await mongoose.connection.close();
            const res = await request(server).get(`/user/${basicUserMock.username}`).set("Authorization", "JWT " + accessToken);

            expect(res.statusCode).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
        })
    })

    describe(('get my user'), () => {

        test('succeed get my user', async () => {
            accessToken = (await request(server).post("/auth/register").send(userToRegister)).body.accessToken;
            const res = await request(server).get(`/user/me`).set("Authorization", "JWT " + accessToken);
            expect(res.statusCode).toBe(StatusCodes.OK);
            expect(res.body.email).toEqual(basicUserMock.email);
            expect(res.body.username).toEqual(basicUserMock.username);
            expect(res.body.bio).toEqual(basicUserMock.bio);
            expect(res.body.imgUrl).toEqual(basicUserMock.imgUrl);
        });

        test('my user not found', async () => {
            const res = await request(server).get(`/user/me`).set("Authorization", "JWT " + accessToken);

            expect(res.statusCode).toBe(StatusCodes.NOT_FOUND);
        })

        test('failed get my user', async () => {
            await mongoose.connection.close();
            const res = await request(server).get(`/user/me`).set("Authorization", "JWT " + accessToken);

            expect(res.statusCode).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
        })
    });

    describe(('edit user'), () => {
        beforeEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            accessToken = (await request(server).post("/auth/register").send(userToRegister)).body.accessToken;
        })

        describe('succeed edit user', () => {

            test(('edit user bio'), async () => {
                const res = await request(server).post('/user/edit').set("Authorization", "JWT " + accessToken).send(editMock);

                expect(res.statusCode).toBe(StatusCodes.OK);
                expect(res.body.bio).toEqual(editMock.bio);
            });

            test('edit user image', async () => {
                const res = await request(server).post('/user/edit')
                    .set("Authorization", "JWT " + accessToken)
                    .attach('image', IMAGE_PATH)

                expect(res.status).toEqual(StatusCodes.OK);
                expect(res.body.imgUrl).toContain(process.env.UPLOADS_PATH);
            });
        });

        describe('failed edit user', () => {

            test(('invalid edit params'), async () => {
                const res = await request(server).post('/user/edit').set("Authorization", "JWT " + accessToken).send({ ...editMock, wrongProp: 'wrong' });
                expect(res.statusCode).toBe(StatusCodes.BAD_REQUEST);
                expect(res.text).toBe("invalid edit params");
            });

            test('user not found', async () => {
                await UserModel.deleteOne({ 'username': userToRegister.username });
                const res = await request(server).post(`/user/edit`).set("Authorization", "JWT " + accessToken).send(editMock);
                
                expect(res.statusCode).toBe(StatusCodes.NOT_FOUND);
            });

            test('failed', async () => {
                await mongoose.connection.close();
                const res = await request(server).post(`/user/edit`).set("Authorization", "JWT " + accessToken).send(editMock);
                
                expect(res.statusCode).toBe(StatusCodes.INTERNAL_SERVER_ERROR);
            });
        });
    });
});