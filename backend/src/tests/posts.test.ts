import { Express } from "express-serve-static-core";
import { StatusCodes } from "http-status-codes";
import mongoose from "mongoose";
import request from 'supertest';
import { PostModel } from "../models/posts.model";
import { UserModel } from "../models/users.model";
import { initServer } from "../server";
import { addPost } from "../services/posts.service";
import { CommentToAdd, EditPostData, Post } from "../types/posts.types";
import { POST_MOCK, UPLOAD_POST_REQUEST_MOCK } from "./posts.mocks";
import { basicUserMock2, userToRegister } from "./users.mock";

let server: Express;
let accessToken: string;
let postId: string
const IMAGE_PATH = 'public/userDefault.jpg';

beforeAll(async () => {
    server = await initServer();
    await mongoose.connect(process.env.DB_URL);
    await UserModel.deleteMany();
    accessToken = (await request(server).post("/auth/register").send(userToRegister)).body.accessToken;
});

beforeEach(async () => {
    await mongoose.connect(process.env.DB_URL);

})

afterAll(async () => {
    await PostModel.deleteMany();
    await UserModel.deleteMany();
    await mongoose.connection.close();
});

describe('posts router', () => {

    afterEach(async () =>
        await mongoose.connect(process.env.DB_URL)
    );

    afterAll(async () => {
        await PostModel.deleteMany();
    });

    describe('get all posts', () => {

        test('succeed get all posts', async () => {
            await PostModel.deleteMany();
            const { _id } = await addPost(POST_MOCK);
            const res = await request(server).get('/posts').set("Authorization", "JWT " + accessToken);
            expect(res.statusCode).toEqual(StatusCodes.OK);
            expect(JSON.parse(res.text)).toEqual([{ _id: _id.toString(), ...POST_MOCK }]);
        });
    })

    describe('get post by id', () => {
        test('succeed get post', async () => {
            const { _id } = await addPost(POST_MOCK);
            const res = await request(server).get(`/posts/${_id.toString()}`).set("Authorization", "JWT " + accessToken);
            expect(res.statusCode).toEqual(StatusCodes.OK);
            expect(JSON.parse(res.text)).toEqual({ _id: _id.toString(), ...POST_MOCK });
        });

        test('failed get post', async () => {
            const { _id } = await addPost(POST_MOCK);
            await mongoose.connection.close();
            const res = await request(server).get(`/posts/${_id.toString()}`).set("Authorization", "JWT " + accessToken);
            expect(res.statusCode).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
        });
    })


    describe('upload post', () => {

        test('succeed upload post', async () => {
            const res = await request(server).post('/posts/upload')
                .set("Authorization", "JWT " + accessToken)
                .attach('image', IMAGE_PATH)
                .field('publisher', UPLOAD_POST_REQUEST_MOCK.publisher)
                .field('description', UPLOAD_POST_REQUEST_MOCK.description)
                .field('plantType', UPLOAD_POST_REQUEST_MOCK.plantType)

            expect(res.status).toEqual(StatusCodes.OK);
            expect(res.body.publisher).toEqual(UPLOAD_POST_REQUEST_MOCK.publisher);
            expect(res.body.plantType).toEqual(UPLOAD_POST_REQUEST_MOCK.plantType);
            expect(res.body.description).toEqual(UPLOAD_POST_REQUEST_MOCK.description);
        })

        describe('failed upload post', () => {

            test('without image', async () => {
                const res = await request(server).post('/posts/upload')
                    .set("Authorization", "JWT " + accessToken).send(UPLOAD_POST_REQUEST_MOCK);

                expect(res.status).toEqual(StatusCodes.BAD_REQUEST);
                expect(res.text).toBe('missing image');
            })

            test('user unauthorized', async () => {
                const res = await request(server).post('/posts/upload')
                    .set("Authorization", "JWT " + accessToken)
                    .attach('image', IMAGE_PATH)
                    .field('publisher', basicUserMock2.username)
                    .field('description', UPLOAD_POST_REQUEST_MOCK.description)
                    .field('plantType', UPLOAD_POST_REQUEST_MOCK.plantType)

                expect(res.status).toEqual(StatusCodes.FORBIDDEN);
                expect(res.text).toBe('user unauthorized');
            })

            test('failed', async () => {
                await mongoose.connection.close();
                const res = await request(server).post('/posts/upload')
                    .set("Authorization", "JWT " + accessToken)
                    .attach('image', IMAGE_PATH)
                    .field('publisher', UPLOAD_POST_REQUEST_MOCK.publisher)
                    .field('description', UPLOAD_POST_REQUEST_MOCK.description)
                    .field('plantType', UPLOAD_POST_REQUEST_MOCK.plantType)

                expect(res.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
                expect(res.text).toBe('post upload failed');
            })
        })
    })

    describe('edit post', () => {

        beforeEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await PostModel.deleteMany();
            postId = (await PostModel.create(POST_MOCK)).toObject()._id.toString();
        });

        afterEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await PostModel.deleteMany();
        }
        );

        describe('succeed edit post', () => {

            test('edit post description', async () => {
                const editParams: EditPostData = {
                    postId: postId,
                    description: 'edit description'
                }
                const res = await request(server).post('/posts/edit')
                    .set("Authorization", "JWT " + accessToken)
                    .send(editParams)

                expect(res.status).toEqual(StatusCodes.OK);
                expect(res.body.description).toEqual(editParams.description);
            })

            test('edit post image', async () => {
                const res = await request(server).post('/posts/edit')
                    .set("Authorization", "JWT " + accessToken)
                    .attach('image', IMAGE_PATH)
                    .field('postId', postId)

                expect(res.status).toEqual(StatusCodes.OK);
                expect(res.body.imagePath).toContain(process.env.UPLOADS_PATH);
            })
        })

        describe('failed edit post', () => {

            test('user unauthorized', async () => {
                await PostModel.updateOne({ "_id": postId }, { "publisher": "wrong" });
                const editParams: EditPostData = {
                    postId: postId,
                    description: 'edit description'
                }
                const res = await request(server).post('/posts/edit')
                    .set("Authorization", "JWT " + accessToken)
                    .send(editParams)

                expect(res.status).toEqual(StatusCodes.FORBIDDEN);
                expect(res.text).toBe('user unauthorized');
            })

            test('failed', async () => {
                await mongoose.connection.close();
                const editParams: EditPostData = {
                    postId: postId,
                    description: 'edit description'
                }
                const res = await request(server).post('/posts/edit')
                    .set("Authorization", "JWT " + accessToken)
                    .send({ editParams })

                expect(res.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
                expect(res.text).toBe('edit post failed');
            })
        })
    })

    describe('delete post', () => {

        beforeEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await PostModel.deleteMany();
            postId = (await PostModel.create(POST_MOCK)).toObject()._id.toString();
        });

        afterEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await PostModel.deleteMany();
        }
        );

        test('succeed delete post', async () => {
            const res = await request(server).delete(`/posts/${postId}`).set("Authorization", "JWT " + accessToken);
            const post = await PostModel.findOne({ '_id': postId });

            expect(res.status).toEqual(StatusCodes.OK);
            expect(post).toBeNull();
        })

        describe('failed delete post', () => {

            test('post not found', async () => {
                await PostModel.deleteOne({ "_id": postId });
                const res = await request(server).delete(`/posts/${postId}`).set("Authorization", "JWT " + accessToken);

                expect(res.status).toEqual(StatusCodes.NOT_FOUND);
                expect(res.text).toBe('post not found')
            })

            test('user unauthorized', async () => {
                await PostModel.updateOne({ "_id": postId }, { "publisher": "wrong" });
                const res = await request(server).delete(`/posts/${postId}`).set("Authorization", "JWT " + accessToken);

                expect(res.status).toEqual(StatusCodes.FORBIDDEN);
                expect(res.text).toBe('user unauthorized');
            })

            test('failed', async () => {
                await mongoose.connection.close();
                const res = await request(server).delete(`/posts/${postId}`).set("Authorization", "JWT " + accessToken);

                expect(res.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
                expect(res.text).toBe('delete post failed');
            })
        })
    })

    describe('add comment', () => {

        test('succeed add comment', async () => {
            const postId = (await PostModel.create(POST_MOCK)).toObject()._id.toString();
            const comment: CommentToAdd = {
                postId,
                commenter: userToRegister.username,
                description: 'test'
            }

            const res = await request(server).post('/posts/comment').set("Authorization", "JWT " + accessToken).send(comment);
            const newComment = (res.body as Post).comments[(res.body as Post).comments.length - 1];

            expect(res.status).toEqual(StatusCodes.OK);
            expect(newComment.commenter).toEqual(comment.commenter);
            expect(newComment.description).toEqual(comment.description);
        })

        test('failed add comment', async () => {
            const comment: CommentToAdd = {
                postId,
                commenter: userToRegister.username,
                description: 'test'
            }
            await mongoose.connection.close();
            const res = await request(server).post('/posts/comment').set("Authorization", "JWT " + accessToken).send(comment);

            expect(res.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
            expect(res.text).toEqual('add comment failed');
        })
    })

    describe('get all user posts', () => {

        test('succeed get all user posts', async () => {
            await PostModel.deleteMany();
            await PostModel.insertMany([POST_MOCK, POST_MOCK, POST_MOCK]);

            const res = await request(server).get(`/posts/byUser/${POST_MOCK.publisher}`).set("Authorization", "JWT " + accessToken);

            expect(res.status).toEqual(StatusCodes.OK);
            expect(res.body.length).toBe(3);
        })

        test('user doesnt exist', async () => {
            const res = await request(server).get(`/posts/byUser/test`).set("Authorization", "JWT " + accessToken);

            expect(res.status).toEqual(StatusCodes.OK);
            expect(res.body.length).toBe(0);
        })

        test('failed get all user posts', async () => {
            await mongoose.connection.close();
            const res = await request(server).get(`/posts/byUser/${POST_MOCK.publisher}`).set("Authorization", "JWT " + accessToken);

            expect(res.status).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
            expect(res.text).toBe('get user posts failed');
        })
    })
});