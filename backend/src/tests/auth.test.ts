import { Express } from "express";
import { StatusCodes } from "http-status-codes";
import mongoose from "mongoose";
import request from "supertest";
import { DEFAULT_USER_IMG_URL } from "../controllers/auth.controller";
import { UserModel } from "../models/users.model";
import { initServer } from "../server";
import { basicUserMock, userToLogin, userToRegister } from "./users.mock";

let server: Express;
let refreshToken;

beforeAll(async () => {
    server = await initServer();
    await UserModel.deleteMany();
});

afterAll(async () => {
    await mongoose.connect(process.env.DB_URL);
    await UserModel.deleteMany();
    await mongoose.connection.close();
});

describe("Auth tests", () => {

    describe("register", () => {

        beforeEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await UserModel.deleteMany();
        });

        afterEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await UserModel.deleteMany();
        });

        test("succeed register", async () => {
            const response = await request(server)
                .post("/auth/register")
                .send(userToRegister);

            expect(response.statusCode).toEqual(StatusCodes.CREATED);
            expect(response.body.email).toEqual(userToRegister.email);
            expect(response.body.username).toEqual(userToRegister.username);
            expect(response.body.bio).toEqual('');
            expect(response.body.imgUrl).toEqual(DEFAULT_USER_IMG_URL);
        });

        describe('failed register', () => {

            test('register with already existing email', async () => {
                await UserModel.create(basicUserMock);
                const response = await request(server)
                    .post("/auth/register")
                    .send(userToRegister);

                expect(response.statusCode).toBe(StatusCodes.CONFLICT);
                expect(response.text).toBe("email already exists");
            });

            test('register with already existing username', async () => {
                await UserModel.create(basicUserMock);
                const response = await request(server)
                    .post("/auth/register")
                    .send({
                        ...userToRegister,
                        email: 'testUser2@test.com'
                    });
                expect(response.statusCode).toBe(StatusCodes.CONFLICT);
                expect(response.text).toBe("username already exists");
            });

            test('register with missing fields', async () => {
                const response = await request(server).post("/auth/register").send({});

                expect(response.statusCode).toBe(StatusCodes.BAD_REQUEST);
                expect(response.text).toBe("register details are missing");
            });

            test('failed', async () => {
                await mongoose.connection.close();
                const response = await request(server)
                    .post("/auth/register")
                    .send(userToRegister);

                expect(response.statusCode).toEqual(StatusCodes.INTERNAL_SERVER_ERROR);
                expect(response.text).toEqual("registration failed");
            });
        });
    });


    describe("login", () => {

        beforeAll(async () => {
            await mongoose.connect(process.env.DB_URL);
            await request(server).post("/auth/register").send(userToRegister);
        });

        afterAll(async () => {
            await mongoose.connect(process.env.DB_URL);
            await UserModel.deleteMany();
        });

        test("succeed login", async () => {
            const response = await request(server)
                .post("/auth/login").send(userToLogin);

            const accessToken = response.body.accessToken;
            const refreshToken = response.body.refreshToken;

            expect(response.statusCode).toBe(StatusCodes.OK);
            expect(response.body.username).toEqual(userToRegister.username);
            expect(response.body.email).toEqual(userToRegister.email);
            expect(response.body.bio).toEqual(userToRegister.bio);
            expect(accessToken).toBeDefined();
            expect(refreshToken).toBeDefined();
        });

        describe('failed login', () => {

            test('login params are missing', async () => {
                const response = await request(server)
                    .post("/auth/login").send({ email: 'test@email.com' });

                expect(response.statusCode).toBe(StatusCodes.BAD_REQUEST);
                expect(response.text).toBe("login details are missing");
            });

            test('incorrect email or password', async () => {
                const response = await request(server)
                    .post("/auth/login").send({ ...userToLogin, password: 'wrong' });

                expect(response.statusCode).toBe(StatusCodes.UNAUTHORIZED);
                expect(response.text).toBe("incorrect email or password");
            });

            test('failed', async () => {
                await mongoose.connection.close();
                const response = await request(server).post("/auth/login").send(userToLogin);

                expect(response.statusCode).toBe(StatusCodes.INTERNAL_SERVER_ERROR);
                expect(response.text).toBe("login failed");
            });
        });
    });

    describe('logout', () => {

        beforeEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            refreshToken = (await request(server).post("/auth/register").send(userToRegister)).body.refreshToken;
        });

        afterEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await UserModel.deleteMany();
        });

        test('succeed logout', async () => {
            const response = await request(server).post("/auth/logout").set("Authorization", "JWT " + refreshToken);

            expect(response.statusCode).toBe(StatusCodes.OK);
        });

        describe('failed logout', () => {

            test('refresh token missing', async () => {
                const response = await request(server).post("/auth/logout");

                expect(response.statusCode).toBe(StatusCodes.UNAUTHORIZED);
            });

            test('user doesnt exist', async () => {
                await UserModel.deleteOne({ 'username': userToRegister.username });
                const response = await request(server).post("/auth/logout").set("Authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.FORBIDDEN);
                expect(response.text).toBe('invalid request');
            });

            test('invalid refresh token', async () => {
                await UserModel.updateOne({ 'username': userToRegister.username }, { 'refreshTokens': [] });
                const response = await request(server).post("/auth/logout").set("Authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.FORBIDDEN);
                expect(response.text).toBe('invalid request');
            });

            test('failed replacing refresh token', async () => {
                await mongoose.connection.close();
                const response = await request(server).post("/auth/logout").set("Authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.FORBIDDEN);
                expect(response.text).toBe('failed logout');
            });
        });
    });

    describe('refresh tokens', () => {

        beforeEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            refreshToken = (await request(server).post("/auth/register").send(userToRegister)).body.refreshToken;
        });

        afterEach(async () => {
            await mongoose.connect(process.env.DB_URL);
            await UserModel.deleteMany();
        });

        describe('succeed refresh tokens', () => {

            test('succeed', async () => {
                const response = await request(server).get("/auth/refresh").set("Authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.OK);
                expect(response.body.accessToken).toBeDefined();
                expect(response.body.refreshToken).toBeDefined();
                expect(response.body.email).toEqual(userToRegister.email);
                expect(response.body.username).toEqual(userToRegister.username);
                expect(response.body.bio).toEqual(userToRegister.bio);
            });

            test("invalid refresh tokens after single use", async () => {
                const response = await request(server).get("/auth/refresh").set("authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.OK);

                const responseAfterTokensUsed = await request(server).get("/auth/refresh").set("authorization", "JWT " + refreshToken);
                expect(responseAfterTokensUsed.statusCode).toBe(StatusCodes.FORBIDDEN);
                expect(responseAfterTokensUsed.text).toBe('invalid request');
            });

            test("access token after refresh works", async () => {
                const response = await request(server).get("/auth/refresh").set("authorization", "JWT " + refreshToken);
                expect(response.statusCode).toBe(StatusCodes.OK);
                expect(response.body.accessToken).toBeDefined();
                expect(response.body.refreshToken).toBeDefined();

                const newAccessToken = response.body.accessToken;

                const responseAfterRefresh = await request(server).get(`/posts/`).set("authorization", "JWT " + newAccessToken);
                expect(responseAfterRefresh.statusCode).toBe(StatusCodes.OK);
            });
        });

        describe('failed refresh tokens', () => {

            test('refresh token missing', async () => {
                const response = await request(server).get("/auth/refresh");

                expect(response.statusCode).toBe(StatusCodes.UNAUTHORIZED);
            });

            test('user doesnt exist', async () => {
                await UserModel.deleteOne({ 'username': userToRegister.username });
                const response = await request(server).get("/auth/refresh").set("Authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.FORBIDDEN);
                expect(response.text).toBe('invalid request');
            });

            test('invalid refresh token', async () => {
                await UserModel.updateOne({ 'username': userToRegister.username }, { 'refreshTokens': [] });
                const response = await request(server).get("/auth/refresh").set("Authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.FORBIDDEN);
                expect(response.text).toBe('invalid request');
            });

            test('failed', async () => {
                await mongoose.connection.close();
                const response = await request(server).get("/auth/refresh").set("Authorization", "JWT " + refreshToken);

                expect(response.statusCode).toBe(StatusCodes.FORBIDDEN);
                expect(response.text).toBe('failed refresh tokens');
            });
        });
    });

    // describe('google signin', () => {

    //     test('succeed login', () => {

    //     })

    //     test('succeed register', () => {

    //     })

    //     describe('failed google signin', () => {

    //     })
    // })

    describe("auth middleware", () => {
        let accessToken: string;

        beforeAll(async () => {
            const response = await request(server)
                .post("/auth/register").send(userToRegister);
            accessToken = response.body.accessToken;
        });

        describe("request without token", () => {
            test("unauthorized access without token", async () => {
                const response = await request(server).get(`/posts`);
                expect(response.statusCode).toBe(StatusCodes.UNAUTHORIZED);
            });
        });

        describe("request with token", () => {
            describe("valid token", () => {
                test("request succeeds with status ok", async () => {
                    const response = await request(server)
                        .get(`/posts/`)
                        .set("Authorization", "JWT " + accessToken);
                    expect(response.statusCode).toBe(StatusCodes.OK);
                });
            });

            describe("invalid token", () => {
                test("request fails with status forbidden", async () => {
                    const response = await request(server)
                        .get(`/posts/`)
                        .set("Authorization", "JWT 1" + accessToken);
                    expect(response.statusCode).toBe(StatusCodes.FORBIDDEN);
                });
            });
        });
    });
});
