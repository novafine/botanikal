import { Schema, model } from "mongoose";
import { Comment, Post } from "../types/posts.types";

const commentSchema = new Schema<Comment>({
    commenter: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    }
}, { _id: false });

const postSchema = new Schema<Post>({
    plantType: String,
    description: String,
    publisher: String,
    imagePath: String,
    comments: [commentSchema]
}, { versionKey: false });

export const PostModel = model<Post>('posts', postSchema);