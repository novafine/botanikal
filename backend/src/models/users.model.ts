import mongoose from "mongoose";
import { User } from "../types/users.types";

const userSchema = new mongoose.Schema<User>({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    username: {
        type: String,
        required: true
    },
    imgUrl: {
        type: String,
    },
    bio: {
        type: String,
    },
    refreshTokens: {
        type: [String],
        required: true,
    },
});

export const UserModel = mongoose.model<User>("users", userSchema);