import { Schema, model } from "mongoose";
import { Plant } from "../types/plants.types";

const plantsSchema = new Schema<Plant>({
    discoveryYear: Number,
    family: String,
    name: String
}, { versionKey: false });

export const PlantModel = model<Plant>('plants', plantsSchema);