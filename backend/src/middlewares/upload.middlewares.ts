import { upload } from "../common/multer.config";

export const uploadMiddleware = (req, res, next) => {
    upload.single('image')(req, res, (err) => {
        if (err) {
            return res.status(400).send('error uploading image');
        }
        next();
    });
}