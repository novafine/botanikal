import { Request, Response } from "express";
import { getAllPlantsNames, getRandomPlant } from "../services/plants.service";

export const getAllPlantsNamesHandler = async (req: Request, res: Response) => {
    const allPlantsNames = await getAllPlantsNames();
    res.json(allPlantsNames);
};

export const getRandomPlantHandler = async (req: Request, res: Response) => {
    const randomPlant = await getRandomPlant();
    res.json(randomPlant);
};