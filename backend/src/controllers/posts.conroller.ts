import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { addComment, addPost, deletePost, getAllPosts, getPostById, getPostsByPublisher, updatePost } from "../services/posts.service";
import { CommentToAddSchema, EditPostParams, EditPostSchema, Post, UploadPostData, UploadPostSchema } from "../types/posts.types";
import { validate } from "../common/validator";
import { getUserByUsername } from "../services/user.service";
import { AuthRequest } from "../types/auth.types";

export const getAllPostsHandler = async (req: Request, res: Response) => {
    const allPosts = await getAllPosts();
    res.json(allPosts);
};

export const getPostByIdHandler = async (req: Request, res: Response) => {
    try {
        const postId = req.params.postId; // TODO: Add validation. maybe string?
        const post = await getPostById(postId);
        res.json(post);
    } catch (err) {
        res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
};

export const uploadPostHandler = async (req: AuthRequest, res: Response) => {
    try {
        if (!req.file)
            return res.status(StatusCodes.BAD_REQUEST).send('missing image');

        const { plantType, description, publisher }: UploadPostData = validate(req.body, UploadPostSchema);

        if (!(await isCurrentUser(req.user._id, publisher))) {
            return res.status(StatusCodes.FORBIDDEN).send('user unauthorized');
        }
        const postWithMetadata: Post = {
            plantType,
            description,
            publisher,
            comments: [],
            imagePath: req.file.path,
        };

        const newPost = await addPost(postWithMetadata);
        return res.send(newPost);
    } catch (err) {
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send("post upload failed");
    }
};

export const editPostHandler = async (req: AuthRequest, res: Response) => {
    try {

        const { postId, description } = validate(req.body, EditPostSchema);

        const postToEdit = await getPostById(postId);

        if (!(await (isCurrentUser(req.user._id, postToEdit.publisher)))) {
            return res.status(StatusCodes.FORBIDDEN).send('user unauthorized');
        }

        const editParams: EditPostParams = description !== undefined && description !== null ?
            { postId, fieldsToEdit: { description: description } }
            : { postId, fieldsToEdit: {} };

        if (req.file?.path) {
            editParams.fieldsToEdit.imagePath = req.file.path;
        }

        const updatedPost = await updatePost(editParams);
        return res.send(updatedPost);

    } catch (err) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send('edit post failed');
    }
};

export const deletePostHandler = async (req: AuthRequest, res: Response) => {
    try {

        const { postId } = req.params;
        const post = await getPostById(postId);

        if (!post) {
            return res.status(StatusCodes.NOT_FOUND).send('post not found');
        }

        if (!(await isCurrentUser(req.user._id, post.publisher))) {
            return res.status(StatusCodes.FORBIDDEN).send('user unauthorized');
        }

        await deletePost(postId);

        res.sendStatus(200);
    } catch (err) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send('delete post failed');
    }
};

export const addCommentHandler = async (req: AuthRequest, res: Response) => {
    try {

        const { postId, commenter, description } = validate(req.body, CommentToAddSchema);

        const updatedPost = await addComment({ postId, comment: { commenter, description } });
        return res.send(updatedPost);

    } catch (err) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send('add comment failed');
    }
};

export const getPostsByUserHandler = async (req: AuthRequest, res: Response) => {
    try {
        const { username } = req.params;
        const posts = await getPostsByPublisher(username);
        return res.send(posts);
    } catch (err) {
        return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send('get user posts failed');
    }
};

const isCurrentUser = async (userReqId, userToCheckUsername) => {
    const userToCheck = await getUserByUsername(userToCheckUsername);
    if (userToCheck) {
        return userReqId === userToCheck._id.toString();
    } else {
        return false;
    }
};