import { compare, genSalt, hash } from 'bcrypt';
import { Request, Response } from "express";
import { OAuth2Client } from 'google-auth-library';
import { StatusCodes } from 'http-status-codes';
import jwt from 'jsonwebtoken';
import { UserModel } from '../models/users.model';
import { addUserRefreshToken, clearUserRefreshTokens, setUserRefrehTokens as setUserRefreshTokens } from "../services/auth.service";
import { TokenInfo } from '../types/auth.types';
import { User, UserRegistrationParams, RegisterSchema, UserLoginParams, LoginSchema } from '../types/users.types';
import { getUserByEmail, getUserById, getUserByUsername } from '../services/user.service';
import { validate } from '../common/validator';

export const DEFAULT_USER_IMG_URL = 'userDefault.jpg';

export const generateTokens = async (user: User & Required<{ _id: string; }>) => {
    const accessToken = jwt.sign({ _id: user._id }, process.env.JWT_ACCESS_TOKEN_SECRET, { expiresIn: process.env.JWT_TOKEN_EXPIRATION });
    const refreshToken = jwt.sign({ _id: user._id }, process.env.JWT_REFRESH_TOKEN_SECRET, { jwtid: Date.now().toString() });
    await addUserRefreshToken(user._id, refreshToken); // TODO: check push on null field

    return { accessToken, refreshToken };
};

export const loginHandler = async (req: Request, res: Response) => {

    try {
        const loginParmas: UserLoginParams = validate(req.body, LoginSchema);
        const { email, password } = loginParmas;

        try {
            const user = await getUserByEmail(email);

            if (!user || !(await compare(password, user.password)))
                return res.status(StatusCodes.UNAUTHORIZED).send("incorrect email or password");

            const tokens = await generateTokens(user);

            return res.status(StatusCodes.OK).send({
                ...user,
                ...tokens
            });
        } catch (err) {
            return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send("login failed");
        }
    } catch {
        return res.status(StatusCodes.BAD_REQUEST).send("login details are missing");
    }
};

export const logoutHandler = async (req: Request, res: Response) => {
    const authHeader = req.headers['authorization'];
    const refreshToken = authHeader && authHeader.split(' ')[1];

    if (!refreshToken)
        return res.sendStatus(StatusCodes.UNAUTHORIZED);

    try {
        const { _id }: TokenInfo = <TokenInfo>jwt.verify(refreshToken, process.env.JWT_REFRESH_TOKEN_SECRET);
        const userInDB = await getUserById(_id);

        if (!userInDB)
            return res.status(StatusCodes.FORBIDDEN).send(`invalid request`);

        if (!userInDB.refreshTokens.includes(refreshToken)) {
            await clearUserRefreshTokens(_id);

            return res.status(StatusCodes.FORBIDDEN).send('invalid request');
        }

        const refreshTokensWithoutCurrent =
            userInDB.refreshTokens.filter(token => token !== refreshToken);
        await setUserRefreshTokens(_id, refreshTokensWithoutCurrent);

        return res.sendStatus(StatusCodes.OK);
    } catch (err) {
        return res.status(StatusCodes.FORBIDDEN).send('failed logout');
    }
};

export const registerHandler = async (req: Request, res: Response) => {

    try {
        const registerParams: UserRegistrationParams = validate(req.body as UserRegistrationParams, RegisterSchema);
        const { email, password, username, bio } = registerParams;
        const imgUrl = req.file?.path ?? DEFAULT_USER_IMG_URL;

        try {
            if (await getUserByEmail(email))
                return res.status(StatusCodes.CONFLICT).send("email already exists");
            if (await getUserByUsername(username))
                return res.status(StatusCodes.CONFLICT).send("username already exists");

            const salt = await genSalt(10);
            const encryptedPassword = await hash(password, salt);
            const newUserDocument = await UserModel.create({ email, password: encryptedPassword, username, bio, imgUrl });
            const newUser = newUserDocument.toObject();
            const tokens = await generateTokens(newUser);
            res.status(StatusCodes.CREATED).send(
                {
                    ...newUser,
                    ...tokens
                });
        } catch (err) {
            return res.status(StatusCodes.INTERNAL_SERVER_ERROR).send("registration failed");
        }
    } catch (err) {
        return res.status(StatusCodes.BAD_REQUEST).send("register details are missing");
    }

};

export const refreshTokensHandler = async (req: Request, res: Response) => {
    const authHeader = req.headers['authorization'];
    const refreshToken = authHeader && authHeader.split(' ')[1];

    if (!refreshToken)
        return res.sendStatus(StatusCodes.UNAUTHORIZED);

    try {
        const { _id }: TokenInfo =
            <TokenInfo>jwt.verify(refreshToken, process.env.JWT_REFRESH_TOKEN_SECRET);
        const userInDB = await getUserById(_id);

        if (!userInDB)
            return res.status(StatusCodes.FORBIDDEN).send(`invalid request`);

        if (!userInDB.refreshTokens.includes(refreshToken)) {
            await clearUserRefreshTokens(_id);

            return res.status(StatusCodes.FORBIDDEN).send(`invalid request`);
        }

        const newAccessToken = jwt.sign(
            { _id },
            process.env.JWT_ACCESS_TOKEN_SECRET,
            { expiresIn: process.env.JWT_TOKEN_EXPIRATION }
        );
        const newRefreshToken = jwt.sign({ _id }, process.env.JWT_REFRESH_TOKEN_SECRET, { jwtid: Date.now().toString() });
        const userUpdatedRefreshTokens =
            userInDB.refreshTokens.filter(token => token !== refreshToken);
        userUpdatedRefreshTokens.push(newRefreshToken);
        await setUserRefreshTokens(_id, userUpdatedRefreshTokens);

        return res.status(StatusCodes.OK).send({
            ...userInDB,
            accessToken: newAccessToken,
            refreshToken: newRefreshToken
        });
    } catch (err) {
        return res.status(StatusCodes.FORBIDDEN).send('failed refresh tokens');
    }
};

const client = new OAuth2Client();
export const googleAuthHandler = async (req: Request, res: Response) => {
    try {
        const loginTicket = await client.verifyIdToken({
            idToken: req.body.credential,
            audience: process.env.GOOGLE_CLIENT_ID,
        });
        const payload = loginTicket.getPayload();
        const email = payload?.email;

        if (!email)
            throw new Error('email is required');

        let user = await getUserByEmail(email);

        if (!user) {
            user = (await UserModel.create({
                email,
                password: '-1',
                imgUrl: DEFAULT_USER_IMG_URL,
                bio: '',
                username: payload?.name?.split(' ')[0] + Date.now().toString()
            })).toObject();
        }

        const tokens = await generateTokens(user);
        res.status(StatusCodes.OK).send({
            ...user,
            ...tokens
        });
    } catch (err) {
        return res.status(StatusCodes.BAD_REQUEST).send(err.message);
    }
};