import { Request, Response } from "express";
import { StatusCodes } from "http-status-codes";
import { editUser, getUserById, getUserByUsername } from "../services/user.service";
import { AuthRequest } from "../types/auth.types";
import { validate } from "../common/validator";
import { EditUserParams, EditUserSchema } from "../types/users.types";

export const getUserByUsernameHandler = async (req: Request, res: Response) => {
    const username = req.params.username;
    try {
        const user = await getUserByUsername(username);
        if (user) {
            return res.json(user);
        } else {
            return res.sendStatus(StatusCodes.NOT_FOUND);
        }
    } catch (err) {
        return res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
};

export const getMyUserHandler = async (req: AuthRequest, res: Response) => {
    try {
        const currUser = req.user;
        const user = await getUserById(currUser._id);
        if (user) {
            return res.json(user);
        } else {
            return res.sendStatus(StatusCodes.NOT_FOUND);
        }
    } catch (err) {
        return res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
    }
};

export const editUserHandler = async (req: AuthRequest, res: Response) => {
    try {
        const { bio }: EditUserParams = validate(req.body as EditUserParams, EditUserSchema);
        const userId = req.user._id;

        try {
            const editParams: { userId: string, fieldsToEdit: EditUserParams } = bio !== undefined && bio !== null ?
                { userId, fieldsToEdit: { bio: bio } }
                : { userId, fieldsToEdit: {} };

            if (req.file?.path) {
                editParams.fieldsToEdit.imgUrl = req.file.path;
            }

            const updatedUser = await editUser(editParams);
            if(updatedUser) {
                return res.send(updatedUser);
            } else {
                return res.sendStatus(StatusCodes.NOT_FOUND);
            }
        } catch (err) {
            return res.sendStatus(StatusCodes.INTERNAL_SERVER_ERROR);
        }
    } catch (err) {
        return res.status(StatusCodes.BAD_REQUEST).send("invalid edit params");
    }
};