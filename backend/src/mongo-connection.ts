import mongoose from "mongoose";

export const connectToMongo = async () => {
    try {
        await mongoose.connect(process.env.DB_URL);
        console.log("Connected to Database");
    } catch (err) {
        console.error("can't connect to mongo", err);
    }
};