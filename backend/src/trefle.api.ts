import axios from "axios";
import { Plant } from "./types/plants.types";
import { updateNewPlants } from "./services/plants.service";
import { setTimeout } from "timers/promises";

interface TreflePlant {
    year: number;
    common_name: string;
    family: string;
    [key: string]: unknown;
}

interface TrefleRawResponse {
    data: TreflePlant[];
    links: {
        self: string;
        first: string;
        last: string;
        next: string;
    };
}

const getRawPlants = async (apiUrl: string): Promise<{ treflePlants: TreflePlant[], nextLink: string; }> => {
    const response = (await axios.get<TrefleRawResponse>(apiUrl)).data;
    const treflePlants = response.data;
    const nextLink = response.links.next;

    return { treflePlants, nextLink };
};

const toPlants = async (treflePlants: TreflePlant[]): Promise<Plant[]> =>
    treflePlants.map(({ common_name, family, year }) => ({
        name: common_name,
        family,
        discoveryYear: year
    }));

const pollPlants = async () => setInterval(async () => {
    try {
        const TOKEN_PARAM = `token=${process.env.TREFLE_API_TOKEN}`;
        let pageNum = 1;
        const API_QUERY = `/api/v1/plants?filter_not[common_name]=null&filter_not[family]=null&filter_not[year]=null`;

        while (pageNum <= 401) {
            try {
                const { treflePlants } = await getRawPlants(`${process.env.TREFLE_API_URL}${API_QUERY}&page=${pageNum}&${TOKEN_PARAM}`);
                const plants: Plant[] = await toPlants(treflePlants);
                await updateNewPlants(plants);
            } catch (err) {
                console.error('poll plants failed', pageNum, err);
            }

            await setTimeout(1000);
            pageNum += 20;
        }
    } catch (err) {
        console.error('poll plants failed', err);
    }
},
    Number(process.env.TREFLE_API_POLLER_INTERVAL_MS)
);

export class PlantsPoller {
    private static pollerInstance: PlantsPoller = null;

    private constructor() {
        pollPlants();
    }

    static startPoller() {
        if (!PlantsPoller.pollerInstance)
            PlantsPoller.pollerInstance = new PlantsPoller();

        return PlantsPoller.pollerInstance;
    }
}