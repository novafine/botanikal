import { UserModel } from "../models/users.model";
import { EditUserParams, User } from "../types/users.types";


export const getUserById = async (userId: string): Promise<User> =>
    UserModel.findById(userId).lean();

export const getUserByEmail = async (email: string) => UserModel.findOne({ email }).lean();

export const getAllUsers = async () => UserModel.find({});

export const getUserByUsername = async (username: string) => UserModel.findOne({ username }).lean();

export const editUser = async (edit: { userId: string, fieldsToEdit: EditUserParams }) =>
    await UserModel.findOneAndUpdate({ "_id": edit.userId }, { $set: edit.fieldsToEdit }, { new: true }).lean();