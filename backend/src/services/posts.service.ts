import { PostModel } from "../models/posts.model";
import { EditPostParams, Post } from "../types/posts.types";

export const getAllPosts = async () =>
    PostModel.find({});

export const getPostById = async (postId: string) =>
    PostModel.findById(postId).lean();

export const addPost = async (post: Post) =>
    PostModel.create(post);

export const updatePost = async (editParams: EditPostParams) =>
    await PostModel.findOneAndUpdate(
        { '_id': editParams.postId },
        { $set: editParams.fieldsToEdit },
        { new: true }
    ).lean();

export const deletePost = async (postId: string) =>
    PostModel.deleteOne({ '_id': postId });

export const addComment = async (comment: { postId: string, comment: { commenter: string, description: string } }) =>
    await PostModel.findOneAndUpdate(
        { '_id': comment.postId },
        { $push: { 'comments': comment.comment } },
        { new: true }
    ).lean();

export const getPostsByPublisher = async (username: string) => 
    await PostModel.find({'publisher': username}).lean();
