import { UserModel } from "../models/users.model";

export const addUserRefreshToken =
    async (userId: string, refreshToken: string) =>
        UserModel.updateOne({ _id: userId }, { $push: { refreshTokens: refreshToken } });

export const clearUserRefreshTokens =
    async (userId: string) =>
        UserModel.updateOne({ _id: userId }, { refreshTokens: [] });

export const setUserRefrehTokens =
    async (userId: string, refreshTokens: string[]) =>
        UserModel.updateOne({ _id: userId }, { refreshTokens });