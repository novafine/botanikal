import { PlantModel } from "../models/plants.model";
import { Plant } from "../types/plants.types";

export const getAllPlantsNames = async () => (await (PlantModel.find({}, { name: 1, _id: 0 }))).map(({ name }) => name);

export const getRandomPlant = async () => {
    const randPlants = await PlantModel.aggregate<Plant>([{ $sample: { size: 1 } }]);

    return randPlants.length > 0 ? randPlants[0] : null;
};

export const updateNewPlants = async (plants: Plant[]) => {
    const bulkUpdates = plants.map(plant => ({
        updateOne: {
            filter: { name: plant.name },
            update: { $setOnInsert: plant },
            upsert: true
        }
    }));

    await PlantModel.bulkWrite(bulkUpdates);
};