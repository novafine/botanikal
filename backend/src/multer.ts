import multer from "multer";

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/');
    },
    filename: function (req, file, cb) {
        const extention = file.originalname.split('.').filter(Boolean).slice(1).join('.');
        cb(null, `${Date.now()}.${extention}`);
    }
});

export const upload = multer({
    storage,
    fileFilter(req, file, cb) {
        if (!file.originalname.toLowerCase().match(/\.(png|jpg|jpeg)$/)) {
            return cb(new Error('file type not supported'));
        }

        cb(null, true);
    }
});