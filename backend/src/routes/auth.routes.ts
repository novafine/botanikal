import { Router } from "express";
import { googleAuthHandler, loginHandler, logoutHandler, refreshTokensHandler, registerHandler } from "../controllers/auth.controller";
import { uploadMiddleware } from "../middlewares/upload.middlewares";

/**
* @swagger
* tags:
*   name: Auth
*   description: The Authentication API
*/

/**
* @swagger
* components:
*   securitySchemes:
*     bearerAuth:
*       type: http
*       scheme: bearer
*       bearerFormat: JWT
*/


/**
* @swagger
* components:
*   schemas:
*     Tokens:
*       type: object
*       required:
*         - accessToken
*         - refreshToken
*       properties:
*         accessToken:
*           type: string
*           description: The JWT access token
*         refreshToken:
*           type: string
*           description: The JWT refresh token
*       example:
*         accessToken: '123cd123x1xx1'
*         refreshToken: '134r2134cr1x3c'
*/

/**
* @swagger
* components:
*   schemas:
*     LoginUser:
*       type: object
*       required:
*         - email
*         - password
*       properties:
*         email:
*           type: string
*           description: The user email
*         password:
*           type: string
*           description: The user password
*       example:
*         email: 'bob@gmail.com'
*         password: '123456'
*/

/**
* @swagger
* components:
*   schemas:
*     GoogleUser:
*       type: object
*       required:
*         - credential
*       properties:
*         credential:
*           type: string
*           description: The user credential id from google
*       example:
*         credential: 'sdjhuncb87sgdjhsb7'
*/

/**
* @swagger
* components:
*   schemas:
*     RegisterUser:
*       type: object
*       required:
*         - email
*         - password
*         - username
*       optional:
*         - bio
*         - image
*       properties:
*         email:
*           type: string
*           description: The user email
*         password:
*           type: string
*           description: The user password
*         username:
*           type: string
*           description: The username
*         bio:
*           type: string
*           description: The users bio
*         image:
*           type: file
*           description: The user profile picture
*       example:
*         email: 'bob@gmail.com'
*         password: '123456'
*         username: 'bob14'
*         bio: 'hi I love plants'
*/

export const createAuthRouter = () => {
    const authRouter = Router();

    /**
    * @swagger
    * /auth/login:
    *   post:
    *     summary: login user
    *     tags: [Auth]
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/LoginUser'
    *     responses:
    *       200:
    *         description: The access and refresh tokens
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/Tokens'
    *       400:
    *         description: Login details are missing
    */
    authRouter.post('/login', loginHandler);

    /**
    * @swagger
    * /auth/register:
    *   post:
    *     summary: registers a new user
    *     tags: [Auth]
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/RegisterUser'
    *     responses:
    *       201:
    *         description: The new user
    *         content:
    *           application/json:
    *             schema:
    *               allOf:
    *                  - $ref: '#/components/schemas/RegisterUser'
    *                  - $ref: '#/components/schemas/Tokens'
    *       400:
    *         description: Register details missing
    *       409:
    *         description: The email or username already taken
    */
    authRouter.post('/register', uploadMiddleware, registerHandler);

    /**
    * @swagger
    * /auth/google:
    *   post:
    *     summary: registers a new user via google provider
    *     tags: [Auth]
    *     requestBody:
    *       required: true
    *       content:
    *         application/json:
    *           schema:
    *             $ref: '#/components/schemas/GoogleUser'
    *     responses:
    *       200:
    *         description: The new user
    *         content:
    *           application/json:
    *             schema:
    *               allOf:
    *                  - $ref: '#/components/schemas/RegisterUser'
    *                  - $ref: '#/components/schemas/Tokens'
    *       400:
    *         description: Register details missing
    */
    authRouter.post('/google', googleAuthHandler);

    /**
     *   @swagger
     *   /auth/logout:
     *     post:
     *       summary: logout a user
     *       tags: [Auth]
     *       description: need to provide the refresh token in the auth header
     *       security:
     *         - bearerAuth: [JWT x]
     *       responses:
     *         200:
     *           description: logout completed successfully
     *         403:
     *           description: invalid request
    */
    authRouter.post('/logout', logoutHandler);

    /**
     * @swagger
     * /auth/refresh:
     *   post:
     *     summary: refresh user tokens
     *     tags: [Auth]
     *     description: need to provide the refresh token in the auth header
     *     security:
     *         - bearerAuth: [JWT x]
     *     responses:
     *       200:
     *         description: The user with new access and refresh tokens
     *         content:
     *           application/json:
     *             schema:
     *               allOf:
     *                  - $ref: '#/components/schemas/RegisterUser'
     *                  - $ref: '#/components/schemas/Tokens'
     *       403:
     *         description: Invalid request
    */
    authRouter.get('/refresh', refreshTokensHandler);

    return authRouter;
}; 