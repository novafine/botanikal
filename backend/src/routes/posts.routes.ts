import { Router } from "express";
import { addCommentHandler, deletePostHandler, editPostHandler, getAllPostsHandler, getPostByIdHandler, getPostsByUserHandler, uploadPostHandler } from "../controllers/posts.conroller";
import { uploadMiddleware } from "../middlewares/upload.middlewares";

/**
 * @swagger
 * tags:
 *  name: Posts
 *  description: The posts API
 */

/**
* @swagger
* components:
*   securitySchemes:
*     bearerAuth:
*       type: http
*       scheme: bearer
*       bearerFormat: JWT
*/

/**
* @swagger
* components:
*   schemas:
*     Post:
*       type: object
*       required:
*         - plantType
*         - description
*         - publisher
*         - imagePath
*         - comments
*       properties:
*         plantType:
*           type: string
*           description: The type of the plant in the post
*         description:
*           type: string
*           description: The description of the post
*         publisher:
*           type: string
*           description: The publisher of the post
*         imagePath:
*           type: string
*           description: The path where the image is saved
*         comments:
*           type: array
*           items:
*             $ref: '#/components/schemas/Comment'
*           description: The comments on the post
*       example:
*         plantType: "daffodil"
*         description: "very pretty"
*         publisher: "user1"
*         imagePath: "user-uploads/dafodill.jpg"
*         comments: []
*     Comment:
*       type: object
*       required:
*         - commenter
*         - description
*       properties:
*         commenter:
*           type: string
*           description: The user that wrote the comment
*         description:
*           type: string
*           description: The content of the comment
*       example:
*         commenter: "user1"
*         description: "great website they should totally get 100"
*     PostToUpload:
*       type: object
*       required:
*         - plantType
*         - description
*         - publisher
*         - image
*       properties:
*         plantType:
*           type: string
*           description: The type of the plant in the post
*         description:
*           type: string
*           description: The description of the post
*         publisher:
*           type: string
*           description: The publisher of the post
*         image:
*           type: file
*           description: The image of the post
*       example:
*         plantType: "daffodil"
*         description: "very pretty"
*         publisher: "user1"
*         image: File
*     PostToEdit:
*       type: object
*       required:
*         - postId
*       properties:
*         postId:
*           type: string
*           description: The Id of the post to edit
*         description:
*           type: string
*           description: The edited description of the post
*         image:
*           type: file
*           description: The new image of the post
*       example:
*         postId: "12345678"
*         description: "this is edited"
*         image: File
*/

/** 
* @swagger
*     /posts/:
*     get:
*       summary: Get all posts
*       tags: [Posts]
*       description: need to provide the refresh token in the auth header
*       security:
*           - bearerAuth: [JWT x]
*       responses:
*         '200':
*           description: Got all posts
*           content:
*             application/json:
*               schema:
*                 type: array
*                 items:
*                   $ref: '#/components/schemas/Post'

* @swagger
*     /posts/{postId}:
*     get:
*       summary: Get single post
*       parameters:
*        - in: path
*          name: postId
*          schema:
*            type: string
*          required: true
*          description: The Id of the requested post
*       tags: [Posts]
*       description: need to provide the refresh token in the auth header
*       security:
*           - bearerAuth: [JWT x]
*       responses:
*         '200':
*           description: Got post
*           content:
*             application/json:
*               schema:
*                 type: array
*                 items:
*                   $ref: '#/components/schemas/Post'
*
* @swagger
* posts/upload:
*    post:
*      summary: "Upload a new post"
*      tags: [Posts]
*      description: need to provide the refresh token in the auth header
*      security:
*           - bearerAuth: [JWT x]
*      requestBody:
*        required: true
*        content:
*          multipart/form-data:
*            schema:
*              $ref: '#/components/schemas/PostToUpload'
*      responses:
*        '200':
*          description: "Uploaded post"
*          content:
*            application/json:
*              schema:
*                type: object
*                $ref: "#/components/schemas/Post"
* @swagger
*  /posts/edit:
*    post:
*      summary: "Edit post"
*      tags: [Posts]
*      description: need to provide the refresh token in the auth header
*      security:
*           - bearerAuth: [JWT x]
*      requestBody:
*        required: true
*        content:
*          multipart/form-data:
*            schema:
*              $ref: '#/components/schemas/PostToEdit'
*      responses:
*        '200':
*          description: "Edited post"
*          content:
*            application/json:
*              schema:
*                type: object
*                $ref: "#/components/schemas/Post"
* @swagger
*     /posts/{postId}:
*     delete:
*       summary: delete single post
*       parameters:
*        - in: path
*          name: postId
*          schema:
*            type: string
*          required: true
*          description: The Id of the post to delete
*       tags: [Posts]
*       description: need to provide the refresh token in the auth header
*       security:
*           - bearerAuth: [JWT x]
*       responses:
*         '200':
*           description: post deleted
*
* @swagger
*  /posts/comment:
*   post:
*     summary: "Add comment to a post"
*     tags: [Posts]
*     description: need to provide the refresh token in the auth header
*     security:
*         - bearerAuth: [JWT x]
*     requestBody:
*       required: true
*       content:
*         application/json:
*           schema:
*             type: "object"
*             required:
*               - postId
*               - commenter
*               - description
*             properties:
*               postId:
*                 type: "string"
*                 description: "The id of the post to comment about"
*               commenter:
*                 type: "string"
*                 description: "The user that wrote the comment"
*               description:
*                 type: "string"
*                 description: "The content of the comment"
*     example:
*       postId: '1234568'
*       commenter: "user1"
*       description: "great website they should totally get 100"
*     responses:
*       200:
*         description: "The new post"
*         content:
*           application/json:
*             schema:
*               $ref: "#/components/schemas/Post"
* @swagger
*     /posts/byUser/{username}:
*     get:
*       summary: get all posts by user
*       parameters:
*        - in: path
*          name: username
*          schema:
*            type: string
*          required: true
*          description: The username of the user to get posts
*       tags: [Posts]
*       description: need to provide the refresh token in the auth header
*       security:
*           - bearerAuth: [JWT x]
*       responses:
*         '200':
*           description: Got all user posts
*           content:
*             application/json:
*               schema:
*                 type: array
*                 items:
*                   $ref: '#/components/schemas/Post'
*/

export const createPostsRouter = () => {
    const postsRouter = Router();

    postsRouter.get('/', getAllPostsHandler);
    postsRouter.get('/:postId', getPostByIdHandler);
    postsRouter.post('/upload', uploadMiddleware, uploadPostHandler);
    postsRouter.post('/edit', uploadMiddleware, editPostHandler);
    postsRouter.delete('/:postId', deletePostHandler);
    postsRouter.post('/comment', addCommentHandler);
    postsRouter.get('/byUser/:username', getPostsByUserHandler);

    return postsRouter;
}; 