import { Router } from "express";
import { getAllPlantsNamesHandler, getRandomPlantHandler } from "../controllers/plants.controller";

/**
 * @swagger
 * tags:
 *  name: Plants
 *  description: The plants API
 */

/**
* @swagger
* components:
*   schemas:
*     Plant:
*       type: object
*       required:
*         - name
*         - family
*         - discoveryYear
*       properties:
*         name:
*           type: string
*           description: The name of the plant
*         family:
*           type: string
*           description: The family of the plant origin
*         discoveryYear:
*           type: number
*           description: The year of the discovery of the plant
*       example:
*         name: 'Apple'
*         family: 'appletians'
*         discoveryYear: '1795'
*/

/**
* @swagger
* components:
*   schemas:
*     PlantsNames:
*       type: array<string>
*       example:
*         ['plantName1', 'plantName2', 'plantName3']
*/

export const createPlantsRouter = () => {
    const plantsRouter = Router();

    /**
    * @swagger
    * /plants/plantsNames:
    *   get:
    *     summary: returns the names of all the plants
    *     tags: [Plants]
    *     description: need to provide the access token in the auth header
    *     security:
    *         - bearerAuth: [JWT x]
    *     responses:
    *       200:
    *         description: The plants names
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/PlantsNames'
    */
    plantsRouter.get('/plantsNames', getAllPlantsNamesHandler);

    /**
    * @swagger
    * /plants/randomPlant:
    *   get:
    *     summary: returns a random plant
    *     tags: [Plants]
    *     description: need to provide the access token in the auth header
    *     security:
    *         - bearerAuth: [JWT x]
    *     responses:
    *       200:
    *         description: The random plant
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/Plant'
    */
    plantsRouter.get('/randomPlant', getRandomPlantHandler);

    return plantsRouter;
}; 