import { Router } from "express";
import { editUserHandler, getMyUserHandler, getUserByUsernameHandler } from "../controllers/user.controller";
import { uploadMiddleware } from "../middlewares/upload.middlewares";

/**
 * @swagger
 * tags:
 *  name: User
 *  description: The user API
 */

/**
* @swagger
* components:
*   securitySchemes:
*     bearerAuth:
*       type: http
*       scheme: bearer
*       bearerFormat: JWT
*/


export const createUserRouter = () => {
    const userRouter = Router();

    /**
* @swagger
* components:
*   schemas:
*     User:
*       type: object
*       required:
*         - email
*         - password
*         - username
*         - imageUrl
*         - refreshTokens
*       optional:
*         - bio
*       properties:
*         email:
*           type: string
*           description: The user email
*         password:
*           type: string
*           description: The user password
*         username:
*           type: string
*           description: The username
*         imageUrl:
*           type: string
*           description: The path to user image
*         refreshTokens:
*           type: array<string>
*           description: The refresh tokens of user
*         bio:
*           type: string
*           description: The user bio
*       example:
*         email: 'bob@gmail.com'
*         password: '123456'
*         username: 'me123'
*         imageUrl: 'user.jpg'
*         refreshTokens: ['zxchhkj.cndkjznc', 'fkzhsdn4ndlfkvzndf']
*         bio: 'hi its me!'
*/

    /**
    * @swagger
    * /user/me:
    *   get:
    *     summary: returns the user of the given token
    *     tags: [User]
    *     description: need to provide the access token in the auth header
    *     security:
    *         - bearerAuth: [JWT x]
    *     responses:
    *       200:
    *         description: The user
    *         content:
    *           application/json:
    *             schema:
    *               $ref: '#/components/schemas/User'
    *       404:
    *         description: user not found
    */
    userRouter.get('/me', getMyUserHandler);

    /**
    * @swagger
    *   /user/:username:
    *   get:
    *     summary: returns the user of the given username
    *     tags: [User]
    *     description: need to provide the access token in the auth header
    *     security:
    *         - bearerAuth: [JWT x]
    *     responses:
    *       200:
    *         description: The user
    *         content:
    *           application/json:
    *              schema:
    *                $ref: '#/components/schemas/User'
    */
    userRouter.get('/:username', getUserByUsernameHandler);

    /**
    * @swagger
    * components:
    *   schemas:
    *     EditUserParams:
    *       type: object
    *       optional:
    *         - bio
    *         - image
    *       properties:
    *         bio:
    *           type: string
    *           description: The user bio
    *         image:
    *           type: file
    *           description: The profile picture file
    *       example:
    *          bio: "its new bio"
    */

    /**
    * @swagger
    *  /user/edit:
    *   post:
    *       summary: edit the info of the user of the given token
    *       tags: [User]
    *       description: need to provide the access token in the auth header
    *       security:
    *           - bearerAuth: [JWT x]
    *       requestBody:
    *         required: true
    *         content:
    *           application/json:
    *              schema:
    *                $ref: '#/components/schemas/EditUserParams'
    *       responses:
    *         200:
    *           description: successfully edited
    */
    userRouter.post('/edit', uploadMiddleware, editUserHandler);

    return userRouter;
}; 