module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    transform: {
      '^.+\\.ts?$': 'ts-jest',
    },
    transformIgnorePatterns: ['backend/node_modules/'],
    testMatch: ["**/?(*.)+(spec|test).ts"]
  };